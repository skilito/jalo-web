<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Cviebrock\EloquentSluggable\Sluggable;


class Catalogue extends Model
{
    use Sluggable;


    protected $table = 'catalogues';

    protected $fillable = ['id', 'prix','quantite','date_ajout','photo',
        'date_fin_validite', 'commercial_id', 'produit_id',
        'fournisseur_id','valide', 'slug', 'pourcentage_boutiquier', 'pourcentage_jalo', 'promo_prix'];

    public $timestamps = false;

    public static function getAllCatalogue($categorie)
    {

        if($categorie == 'all')
        {

        $catalogues = DB::table('catalogues')
            ->join('users', 'users.id', '=', 'catalogues.fournisseur_id')
            ->join('produits as p', 'p.id', '=', 'catalogues.produit_id')
            ->join('categories as c', 'c.id', '=', 'p.categorie_id')
            ->select('catalogues.*', 'p.libelle as productLibelle', 'p.description',
                'c.libelle as libelleCategorie', 'users.nom as nomFournisseur','users.prenom as prenomFournisseur','catalogues.image',
                'users.avatar as avatarFournisseur', 'users.id as fournisseur_id', 'c.icone')->orderBy('catalogues.date_ajout', 'DESC')->paginate(8);
        }

        elseif ($categorie == 'promo')
        {
            $catalogues = DB::table('catalogues')
                ->join('users', 'users.id', '=', 'catalogues.fournisseur_id')
                ->join('produits as p', 'p.id', '=', 'catalogues.produit_id')
                ->join('categories as c', 'c.id', '=', 'p.categorie_id')
                ->select('catalogues.*', 'p.libelle as productLibelle', 'p.description',
                    'c.libelle as libelleCategorie', 'users.nom as nomFournisseur','users.prenom as prenomFournisseur',
                    'users.avatar as avatarFournisseur', 'users.id as fournisseur_id','catalogues.image', 'c.icone')
                ->whereNotNull('promo_prix')->orderBy('catalogues.date_ajout', 'DESC')
                ->paginate(8);
        }
        else
        {
            $catalogues = DB::table('catalogues')
            ->join('users', 'users.id', '=', 'catalogues.fournisseur_id')
            ->join('produits as p', 'p.id', '=', 'catalogues.produit_id')
            ->join('categories as c', 'c.id', '=', 'p.categorie_id')
            ->select('catalogues.*', 'p.libelle as productLibelle','c.libelle as libelleCategorie','p.description', 'users.nom as nomFournisseur','users.prenom as prenomFournisseur',
                'users.avatar as avatarFournisseur', 'users.id as fournisseur_id','catalogues.image', 'c.icone','p.description')
            ->where('c.id', (int)$categorie)->orderBy('catalogues.date_ajout', 'DESC')->paginate(8);
        }

        return $catalogues;
    }

    public static function showcatalogue($catalogue_id)
    {
        return DB::table('catalogues as c')
            ->select('c.id as catalogue_id', 'c.prix', 'c.quantite',
                'c.date_ajout', 'c.photo','c.date_fin_validite', 'c.valide',
                'c.slug as slug_catalogue', 'c.commercial_id', 'c.fournisseur_id',
                'c.produit_id as produit_id', 'p.libelle as libelle_produit', 'p.description',
                'commercial.prenom as prenom_commercial', 'commercial.nom as nom_commercial',
                'commercial.adresse as adresse_commercial', 'commercial.phone1 as phone1_commercial',
                'commercial.avatar as photo_avatar','fournisseur.prenom as prenom_fourniseur',
                'fournisseur.nom as nom_fournisseur', 'fournisseur.adresse as fournisseur_adresse',
                'fournisseur.phone1 as phone1_fournisseur', 'fournisseur.avatar as photo_avatar')
            ->join('produits as p', 'p.id', '=', 'c.produit_id')
            ->join('users as commercial', 'commercial.id', '=', 'c.commercial_id')
            ->join('users as fournisseur', 'fournisseur.id', '=', 'c.fournisseur_id')->where('c.id', '=', $catalogue_id)
            ->get();
    }

    /**
     * Get the indexable data array for the model.
     *
     * @return array
     */
    public function toSearchableArray()
    {
        $array = $this->toArray();

        // Customize array...

        return $array;
    }
    
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'prix'
            ]
        ];
    }
}
