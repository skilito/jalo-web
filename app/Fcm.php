<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fcm extends Model
{
    protected $table = 'fcm';
    protected $fillable = ['user_id', 'token', 'created_at'];
    public $timestamps = false;
}
