<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Role;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Redirect;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    //protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    
   /* public function __construct()
    {
        $this->middleware('guest')->except('logout');
    } */

    public function index()
    {

        return view('auth.login');
    }

    public function store(Request $request)
    {
//        return($request->get('phone1'));
        if (!auth()->attempt(request(['phone1', 'password']))) {
            return 'error';
        }

        if (Role::where('role', 'admin')->first()->id == Auth::user()->role_id)
        {
            return 'admin';
//            return redirect('admin/dashboard');
        }
        if(Role::where('role', 'commercial')->first()->id == Auth::user()->role_id)
        {
             return 'commercial';
//            return redirect()->route('dashbord-commercial', ['category' => 'all']);
        }
        else
        {     return 'client';
//            return redirect()->route('client_catalogues', ['category' => 'all']);
        }
    }

    public function destroy()
    {
      auth()->logout();
      return redirect()->home();
    }
}
