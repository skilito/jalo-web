<?php

namespace App\Http\Controllers;

use App\Etat;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Panier;
use Auth;
use App\Commande;
use App\CatalogueCommande;
use Illuminate\Support\Facades\DB;

class CommandeController extends Controller
{
    
    public function index(Request $request)
    {
//    	$nbr_panier = Panier::where('user_id', Auth::user()->id)->count();
//
//    	$commandes = DB::table('commandes as c')
//                     ->join()
//                     ->where('client_id', '=', Auth::user()->id)->get();

    	//$commandes = Commande::getCommandesToUser($request->get('categorie'));

    	//return response()->json($commandes);

    }

    public function store(Request $request)
    {

        $produits = $request->get('produit');
        $quantites = $request->get('quantite');

        $etat_id = Etat::where('libelle', 'cours')->get()->first()->id;

        $reference = Commande::quickRandom(14);

        $commande = Commande::create(['client_id' => Auth::user()->id,
            'date_commande'=> Carbon::now(), 'etat_id' => $etat_id, 'reference' => $reference]);

        for($i=0; $i < count($produits); $i++)
        {
            $catalogueCommande = new CatalogueCommande();

            $catalogueCommande->commande_id = $commande->id;
            $catalogueCommande->catalogue_id = (int)$produits[$i];
            $catalogueCommande->quantite = (int)$quantites[$i];
            $catalogueCommande->save();

        }
        return 'success';
    }


}
