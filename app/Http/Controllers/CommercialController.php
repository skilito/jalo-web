<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Categorie;
use App\Catalogue;
use Auth;
use Illuminate\Support\Facades\DB;
use App\Role;

class CommercialController extends Controller
{
    public function index(Request $request)
    {

        $fournisseurs = User::where('role_id', Role::where('role', 'fournisseur')->get()->first()->id)->get();

        $categories = Categorie::getAllCategories();

        if($request->get('search') != null)
        {
            $search = $request->get('search');
            $catalogues = DB::table('catalogues as c')->select('p.libelle as produit', 'users.nom', 'users.prenom', 'c.prix', 'c.id', 'c.photo')
                ->join('produits as p', 'p.id', '=', 'c.produit_id')
                ->join('users', 'users.id', '=', 'c.fournisseur_id')
                ->where('p.libelle',$search)
                ->orWhere('users.nom', 'like', '%' . $search . '%')
                ->orWhere('users.prenom', 'like', '%' . $search . '%')
                ->orderBy('date_ajout', 'DESC')
                ->paginate(8);
        }
        else {
            $catalogues = DB::table('catalogues as c')->select('p.libelle as produit', 'users.nom', 'users.prenom', 'c.prix', 'c.id', 'c.photo')
                ->join('produits as p', 'p.id', '=', 'c.produit_id')
                ->join('users', 'users.id', '=', 'c.fournisseur_id')->orderBy('date_ajout', 'DESC')
                ->paginate(8);
        }
        return view('commercial.catalogues')->with(['catalogues' => $catalogues])->with(['categories' => $categories])
            ->with(['fournisseurs' => $fournisseurs]);
    }

    public function getOrdersByCommercail(Request $request)
    {
        if($request->get('search') != null)
        {
            $search = $request->get('search');

            $commandes = DB::table('commandes as c')->select('etats.libelle as etat', 'c.id', 'u.nom as nomBoutiquier',
                'u.prenom as prenomBoutiquier','c.reference', 'c.date_commande', 'u.phone1 as telBoutiquier', 'q.nom as quartier')
                ->leftjoin('boutiques as b', 'b.boutiquier_id', '=', 'c.boutiquier_id')
                ->leftjoin('users as u', 'u.id', '=', 'b.boutiquier_id')
                ->leftjoin('quartiers as q', 'q.id', '=', 'u.quartier_id')
                ->where('b.commercial_id', Auth::user()->id)
                ->leftjoin('etats', 'etats.id', '=', 'c.etat_id')
                ->where('c.reference', $search)
                ->orWhere('u.prenom', 'like', '%' . $search . '%')
                ->orWhere('u.nom', 'like', '%' . $search . '%')
                ->orWhere('q.nom', 'like', '%' . $search . '%')
                ->orderBy('date_commande', 'DESC')->paginate(8);
        }
        else
        {
            $commandes = DB::table('commandes as c')->select('etats.libelle as etat', 'c.id', 'u.nom as nomBoutiquier',
                'u.prenom as prenomBoutiquier','c.reference', 'c.date_commande', 'u.phone1 as telBoutiquier', 'q.nom as quartier')
                ->leftjoin('boutiques as b', 'b.boutiquier_id', '=', 'c.boutiquier_id')
                ->leftjoin('users as u', 'u.id', '=', 'b.boutiquier_id')
                ->leftjoin('quartiers as q', 'q.id', '=', 'u.quartier_id')
                ->where('b.commercial_id', Auth::user()->id)
                ->leftjoin('etats', 'etats.id', '=', 'c.etat_id')
                ->orderBy('date_commande', 'DESC')->paginate(8);
        }

        return view('commercial.commandes')->with(['commandes' => $commandes]);
    }

   public function getClients(Request $request)
   {
       if($request->get('search') != null)
       {
           $search = $request->get('search');
           $clients = DB::table('clients as u')->select('u.nom', 'u.prenom', 'u.adresse', 'u.phone1', 'q.nom as quartier', 'u.id')->orderBy('date_creation', 'DESC')
               ->leftjoin('boutiques as b', 'b.boutiquier_id', '=', 'u.boutiquier_id')
               ->where('u.phone1', 'like', $search)
               ->orWhere('u.prenom', 'like', '%' . $search . '%')
               ->orWhere('u.nom', 'like', '%' . $search . '%')
               ->orWhere('q.nom', 'like', '%' . $search . '%')
               ->join('quartiers as q', 'q.id', '=', 'u.quartier_id')
               ->where('b.commercial_id', Auth::user()->id)->paginate(16);
       }

       else
       {
           $clients = DB::table('clients as u')->select('u.nom', 'u.prenom', 'u.adresse', 'u.phone1', 'q.nom as quartier', 'u.id')->orderBy('date_creation', 'DESC')
               ->leftjoin('boutiques as b', 'b.boutiquier_id', '=', 'u.boutiquier_id')
               ->join('quartiers as q', 'q.id', '=', 'u.quartier_id')
               ->where('b.commercial_id', Auth::user()->id)->paginate(16);

           $produits = DB::table('catalogues as c')->select('c.id', 'c.prix', 'p.libelle as nom')
                        ->join('produits as p', 'p.id', '=', 'c.produit_id')->get();
       }


       return view('commercial.clients')->with(['clients' => $clients])->with(['produits' => $produits]);
   }

    public function getFournisseurs(Request $request)
    {

        $role_id = Role::where('role', 'fournisseur')->get()->first()->id;

        if($request->get('search') != null)
        {
            $search = $request->get('search');

            $fournisseurs = DB::table('users as u')->select('u.nom', 'u.prenom', 'u.adresse', 'u.phone1', 'q.nom as quartier', 'u.id')->orderBy('date_inscription', 'DESC')
                ->leftjoin('quartiers as q', 'q.id', '=', 'u.quartier_id')
                ->where('u.phone1', 'like', $search)
                ->orWhere('u.prenom', 'like', '%' . $search . '%')
                ->orWhere('u.nom', 'like', '%' . $search . '%')
                ->where('u.role_id', $role_id)->paginate(16);
        }
        else
        {
            $fournisseurs = DB::table('users as u')->select('u.nom', 'u.prenom', 'u.adresse', 'u.phone1', 'q.nom as quartier', 'u.id')->orderBy('date_inscription', 'DESC')
                ->leftjoin('quartiers as q', 'q.id', '=', 'u.quartier_id')
                ->where('u.role_id', $role_id)->paginate(16);
        }
        return view('commercial.fournisseurs')->with(['fournisseurs' => $fournisseurs]);
    }

    public function show()
    {
        return view('commercial.showCommande');
    }

    public function showUser($type, $user_id)
    {
        //dd($type);
        $user = DB::table('clients as u')->select('u.nom', 'u.prenom', 'u.nom', 'u.adresse',
            'u.id', 'u.adresse', 'u.phone1', 'u.phone2', 'q.nom as quartier')
            ->join('quartiers as q', 'q.id', '=', 'u.quartier_id')
            ->where('u.id', $user_id)->get()->first();

        return view('commercial/show')->with(['type' => $type])->with(['user' => $user]);
    }

}
