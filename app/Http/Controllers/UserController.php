<?php

namespace App\Http\Controllers;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use App\Role;
use  App\Quartier;
use Illuminate\Support\Facades\DB;


class UserController extends Controller

{
    public function getUsers(Request $request)
    {

        $users = DB::table('users as u')->select('u.nom', 'u.prenom', 'u.date_naissance',
            'u.email','u.sexe','info.label', 'roles.role', 'u.phone1','u.phone2','u.date_inscription')
            ->join('info', 'info.id', '=', 'u.info_id')
            ->join('roles', 'roles.id', '=', 'u.role_id')
            ->where('roles.role', 'fournisseurFMCG')->get();

        return response()->json(['users' => $users, 'status' => 200]);

    }

    public function update(Request $request){

        $client_id = $request->get('user_id');
        $rules = array(
            'nom'       => 'required|min:2',
            'prenom'     => 'required|min:2',
            'email'      => 'required|email',
            'quartier_id'      => 'required',
            'phone1'      => 'required'
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
         return response()->json(['error' =>$validator->errors(), 'status' =>404]);
        } else {

            $user = User::find($client_id);

            $user->nom = $request->get('nom');
            $user->prenom  = $request->get('prenom');
            $user->adresse  = $request->get('adresse');
            $user->phone1  = $request->get('phone1');
            $user->phone2  = $request->get('phone2');
            $user->sexe = $request->get('sexe');
            $user->quartier_id = $request->get('quartier_id');
            $user->date_naissance  = $request->get('date_naissance');

            if($user->save())
            {
                return response()->json(['message' =>'update success', 'status' => 200]);
            }
            else
            {
                return response()->json(['message' =>'update not success', 'status' => 404]);
            }
        }
    }

    public function destroy(Request $request)
    {
        $user = User::find($request->get('id'));

       if($user->delete())
       {
           return response()->json(['message' =>'delete succes', 'status' => 200]);
       }
       else
       {
           return response()->json(['message' =>'error user not be delete', 'status' => 404]);
       }

    }

    public function  getAllRoles()
    {
        $roles = Role::getRoles();

        return response()->json(['roles' =>$roles, 'status' => 200]);
    }



}
