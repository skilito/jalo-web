<?php

namespace App\Http\Controllers\Admin;

use App\Boutique;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Quartier;
use Carbon\Carbon;

class AdminBoutiqueController extends Controller
{
    public function index()
    {
        $boutiquiers = User::where('role_id', Role::where('role', 'boutiquier')->first()->id)->get();
        $commerciaux = User::where('role_id', Role::where('role', 'commercial')->first()->id)->get();
        $quartiers = Quartier::orderBy('nom', 'ASC')->get();
        if(isset($_GET['search'])) {
            $search = $_GET['search'];

            $boutiques = DB::table('boutiques as b')
                ->join('users as bou', 'bou.id', '=', 'b.boutiquier_id')
                ->select('bou.prenom as prenomBoutiquier',
                'bou.nom as nomBoutiquier', 'bou.phone1 as telBoutiquier',
                'com.nom as nomCommercial', 'com.prenom as prenomCommercial', 'com.phone1 as telCommercial', 'q.nom as quartier', 'bou.date_inscription')
                ->where('bou.prenom', 'like', '%' . $search . '%')
                ->orWhere('bou.nom', 'like', '%' . $search . '%')
                ->orWhere('com.prenom', 'like', '%' . $search . '%')
                ->orWhere('com.nom', 'like', '%' . $search . '%')
                ->join('quartiers as q', 'q.id', '=', 'bou.quartier_id')
                ->join('users as com', 'com.id', '=', 'b.commercial_id')->paginate(8);
        }
        elseif (isset($_GET['quartier'])  && $_GET['quartier'] != null )
        {
            $boutiques = DB::table('boutiques as b')
                ->join('users as bou', 'bou.id', '=', 'b.boutiquier_id')
                ->select('bou.prenom as prenomBoutiquier',
                    'bou.nom as nomBoutiquier', 'bou.phone1 as telBoutiquier',
                    'com.nom as nomCommercial', 'com.prenom as prenomCommercial', 'com.phone1 as telCommercial', 'q.nom as quartier', 'bou.date_inscription')
                ->where('q.nom', $_GET['quartier'])
                ->join('users as com', 'com.id', '=', 'b.commercial_id')
                ->join('quartiers as q', 'q.id', '=', 'bou.quartier_id')
                ->paginate(8);
        }
        elseif (isset($_GET['dateDebut']) && $_GET['dateDebut'] != null && $_GET['dateFin'] == null)
        {
            $boutiques = DB::table('boutiques as b')
                ->join('users as bou', 'bou.id', '=', 'b.boutiquier_id')
                ->select('bou.prenom as prenomBoutiquier',
                    'bou.nom as nomBoutiquier', 'bou.phone1 as telBoutiquier',
                    'com.nom as nomCommercial', 'com.prenom as prenomCommercial', 'com.phone1 as telCommercial', 'q.nom as quartier', 'bou.date_inscription')
                ->whereDate('bou.date_inscription', '=', $_GET['dateDebut'])
                ->join('users as com', 'com.id', '=', 'b.commercial_id')
                ->join('quartiers as q', 'q.id', '=', 'bou.quartier_id')
                ->paginate(8);
        }

        elseif ( isset($_GET['dateDebut'])   &&  isset($_GET['dateFin']) &&  $_GET['dateDebut'] != null && $_GET['dateFin'] != null)
        {

            $boutiques = DB::table('boutiques as b')
                ->join('users as bou', 'bou.id', '=', 'b.boutiquier_id')
                ->select('bou.prenom as prenomBoutiquier',
                    'bou.nom as nomBoutiquier', 'bou.phone1 as telBoutiquier',
                    'com.nom as nomCommercial', 'com.prenom as prenomCommercial', 'com.phone1 as telCommercial', 'q.nom as quartier', 'bou.date_inscription')
                ->whereDate('bou.date_inscription', '>=', $_GET['dateDebut'])
                ->whereDate('bou.date_inscription', '<=', $_GET['dateFin'])
                ->join('users as com', 'com.id', '=', 'b.commercial_id')
                ->join('quartiers as q', 'q.id', '=', 'bou.quartier_id')
                ->paginate(8);
        }
        else {
            $boutiques = DB::table('boutiques as b')->join('users as bou', 'bou.id', '=', 'b.boutiquier_id')->select('bou.prenom as prenomBoutiquier',
                'bou.nom as nomBoutiquier', 'bou.phone1 as telBoutiquier',
                'com.nom as nomCommercial', 'com.prenom as prenomCommercial', 'com.phone1 as telCommercial','q.nom as quartier', 'bou.date_inscription')
                ->join('quartiers as q', 'q.id', '=', 'bou.quartier_id')
                ->join('users as com', 'com.id', '=', 'b.commercial_id')->paginate(8);
        }

        return view('admin.boutiques')->with(['commerciaux' => $commerciaux])
            ->with(['boutiquiers' => $boutiquiers])->with(['boutiques' => $boutiques])
            ->with(['quartiers' => $quartiers]);
    }

    public function create(Request $request)
    {

        Boutique::create(['boutiquier_id' => (int)$request->get('boutiquier'), 'commercial_id' => (int)$request->get('commercial')]);
        return 'success';
    }
}
