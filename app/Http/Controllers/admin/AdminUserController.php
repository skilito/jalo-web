<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Info;
use App\MaBoutique;
use App\Quartier;
use App\Role;
use Illuminate\Support\Facades\DB;
use OpenCloud\Common\Exceptions\UnrecognizedServiceError;
use Request;
use Carbon\Carbon;
use App\User;
use App\Client;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;

class AdminUserController extends Controller
{
    public function create_commercial()
    {

        $file_name = 'new_photo';
        $file = Request::file($file_name);

        $email = isset($_POST['new_email']) ? $_POST['new_email']: "" ;


        if($_POST['password'] != $_POST['confirpassword'])
        {
            return 'errorpassword';
//            return response()->json(['passw' => $_POST['password'], 'passCon' => $_POST['confirpassword']]);
        }

        if ($file != null){
            $newFileName = uniqid() . '.' . $file->clientExtension();

            $extension = \File::extension($file_name);

            $file->move(public_path() . '/images/photos', $newFileName);

            $image = 'images/photos/' . $newFileName;
        }
        else
        {
            $image = null;
        }

        $auth = User::where('phone1', $_POST['new_tel'])->get()->first();

        if($auth != null)
        {
          return 'error';
        }
        else {

            $role_id = Role::where('role', 'commercial')->get()->first()->id;

            $info_id = Info::where('label', 'internet')->get()->first()->id;

            $password = bcrypt($_POST['password']);

            $user = User::create(['adresse' => $_POST['new_adresse'], 'email' => $email,
                'prenom' => $_POST['new_prenom'], 'nom' => $_POST['new_nom'], 'phone1' => $_POST['new_tel'], 'role_id' => $role_id, 'password' => $password,
                'info_id' => $info_id, 'avatar' => $image, 'quartier_id' => $_POST['quartier'], 'date_inscription' => Carbon::now()
            ]);

            return 'success';
        }
    }

    public function getClients()
    {
        $quartiers = Quartier::orderBy('nom', 'ASC')->get();

        if(isset($_GET['search'])) {
            $search = $_GET['search'];
            if ($search != null) {
                $clients = DB::table('clients as u')->select('u.nom', 'u.prenom', 'u.adresse', 'phone1', 'q.nom as quartier', 'u.id', 'u.date_creation')
                    ->where('u.nom', $search)
                    ->orWhere('u.prenom', 'like', '%' . $search . '%')
                    ->orWhere('phone1', 'like', '%' . $search . '%')
                    ->orWhere('q.nom', 'like', '%' . $search . '%')
                    ->orderBy('date_creation', 'DESC')
                    ->join('quartiers as q', 'q.id', '=', 'u.quartier_id')->paginate(16);
            }
            else {
                $clients = DB::table('clients as u')->select('u.nom', 'u.prenom', 'u.adresse',
                    'phone1', 'q.nom as quartier', 'u.id', 'u.date_creation')->orderBy('date_creation', 'DESC')
                    ->join('quartiers as q', 'q.id', '=', 'u.quartier_id')->paginate(16);
            }
        }
        elseif (isset($_GET['quartier'])  && $_GET['quartier'] != null )
        {

            $clients = DB::table('clients as u')->select('u.nom', 'u.prenom', 'u.adresse', 'phone1',
                    'q.nom as quartier', 'u.id', 'u.date_creation')->orderBy('date_creation', 'DESC')
                    ->where('q.nom', '=',$_GET['quartier'])
                    ->join('quartiers as q', 'q.id', '=', 'u.quartier_id')->paginate(16);
        }
        elseif (isset($_GET['dateDebut']) && $_GET['dateDebut'] != null && $_GET['dateFin'] == null)
        {
            $date_debut = $_GET['dateDebut'];
            $clients = DB::table('clients as u')->select('u.nom', 'u.prenom', 'u.adresse', 'phone1',
                'q.nom as quartier', 'u.id', 'u.date_creation')
                ->orderBy('date_creation', 'DESC')
                ->whereDate('u.date_creation','=' , $date_debut)
                ->join('quartiers as q', 'q.id', '=', 'u.quartier_id')->paginate(16);

//            return response()->json($clients);

        }

        elseif ( isset($_GET['dateDebut'])   &&  isset($_GET['dateFin']) &&  $_GET['dateDebut'] != null && $_GET['dateFin'] != null)
        {
//                    if($_GET['dateDebut'] != "" || $_GET['dateDebut'] != null  && $_GET['dateFin']!= ""  || $_GET['dateFin'] != null &&  $_GET['dateDebut'] > $_GET['dateFin'])
//                    {
//                        $clients =  ['error' => 'true', 'message' => 'date début supérieur à la date fin'];
//                          return back()->with(['error' => 'true', 'message' => 'date début supérieur à la date fin', 'clients' => $clients]);
////                        return response()->jsonp('ok');
//                    }
//                   else
//                   {
                       $date_debut = $_GET['dateDebut'];
                       $date_fin = $_GET['dateFin'];

                       $clients = DB::table('clients as u')->select('u.nom', 'u.prenom', 'u.adresse', 'phone1',
                           'q.nom as quartier', 'u.id', 'u.date_creation')
                           ->orderBy('u.date_creation', 'DESC')
                           ->whereDate('u.date_creation', '>=', $date_debut)
                           ->whereDate('u.date_creation', '<=', $date_fin)
                           ->join('quartiers as q', 'q.id', '=', 'u.quartier_id')->paginate(16);
//                   }
        }

        else
        {
            $clients = DB::table('clients as u')->select('u.nom', 'u.prenom', 'u.adresse', 'phone1', 'q.nom as quartier', 'u.id', 'u.date_creation')->orderBy('date_creation', 'DESC')
            ->join('quartiers as q', 'q.id', '=', 'u.quartier_id')->paginate(16);
        }

      return view('admin.clients')->with(['clients' => $clients])->with(['quartiers' => $quartiers]);
    }


    public function show($type_user,$client_id)
    {
        $client = User::find($client_id);
        //dd($type_user);

        if($type_user == "client"){

            $type = 'Client';
        }

        if($type_user == "fournisseur"){

            $type = 'Fournisseur';
        }

        if($type_user == "commercial"){

            $type = 'Commercial';
        }
        if($type_user == "boutiquier"){

            $type = 'Boutiquier';
        }

        return view('admin.show')->with(['client' =>$client])->with(['type' => $type]);
    }

    public function edit($type_user,$user_id)
    {
        $client = User::find($user_id);
        if($type_user == "client"){

            $type = 'Client';
        }

        if($type_user == "fournisseur"){

            $type = 'Fournisseur';
        }
        if($type_user == "commercial"){

            $type = 'Commercial';
        }
        if($type_user == "boutiquier"){

            $type = 'Boutiquier';
        }
        return view('admin.show')->with(['client' =>$client])->with(['type' => $type]);
    }

    public function editUser($type_user, $user_id)
    {
        if($type_user == "client"){

            $type = 'Client';
        }
        if($type_user == "fournisseur"){

            $type = 'Fournisseur';
        }

        if($type_user == "commercial"){

            $type = 'Commercial';
        }
        $user= User::find($user_id);
        return view('admin.edit')->with(['user' => $user])->with(['type' => $type]);
    }

    public function update()
    {
        $user_id = (int)Request::input('user_id');

        $user = User::find($user_id);

            $user->nom = Request::input('editUser_nom');
            $user->prenom = Request::input('editUser_prenom');
            $user->phone1 = Request::input('editUser_tel');
            $user->email = Request::input('editUser_email');
            $user->adresse = Request::input('editUser_adresse');

       // return $user;
       $user->save();

        //Session::flash('message', ' Client mis à jour!');

        if(Request::input('type') == 'Clients') {
            return Redirect::to(route('clients'));
        }
        if(Request::input('type') == 'Fournisseur')
        {
            return Redirect::to(route('fournisseurs'));
        }
        if(Request::input('type') == 'Commercial'){
            return Redirect::to(route('commerciaux'));
        }
    }

    public function getAllFournisseurs()
    {
        $quartiers = Quartier::all();
        $role_id = Role::where('role', 'fournisseur')->get()->first()->id;


        if(isset($_GET['search']))
        {
            $search = $_GET['search'];

            $fournisseurs = DB::table('users as u')->select('u.nom', 'u.prenom', 'u.adresse', 'phone1', 'q.nom as quartier', 'u.id','u.date_inscription')->orderBy('date_inscription', 'DESC')
                ->leftjoin('quartiers as q', 'q.id', '=', 'u.quartier_id')
                ->where('u.prenom', $search)
                ->orWhere('u.nom', 'like', '%' . $search . '%')
                ->orWhere('q.nom', 'like', '%' . $search . '%')
                ->where('u.role_id', $role_id)->paginate(16);
        }
        else
            {

            $fournisseurs = DB::table('users as u')->select('u.nom', 'u.prenom', 'u.adresse', 'phone1',
                'q.nom as quartier', 'u.id','u.date_inscription')->orderBy('date_inscription', 'DESC')
                ->leftjoin('quartiers as q', 'q.id', '=', 'u.quartier_id')
                ->where('u.role_id', $role_id)->paginate(16);
        }
        return view('admin.fournisseurs-admin')->with(['clients' => $fournisseurs])->with(['quartiers' => $quartiers]);
    }

    public function getAllCommerciaux()
    {
        $quartiers = Quartier::all();

        $role_id = Role::where('role', 'commercial')->get()->first()->id;

        if(isset($_GET['search'])) {
            $search = $_GET['search'];

            $commerciaux = DB::table('users as u')->select('u.nom', 'u.prenom', 'u.adresse', 'phone1', 'q.nom as quartier', 'u.id', 'u.date_inscription')->orderBy('date_inscription', 'DESC')
                ->leftjoin('quartiers as q', 'q.id', '=', 'u.quartier_id')
                ->where('u.prenom', 'like', '%' . $search . '%')
                ->orWhere('u.nom', 'like', '%' . $search . '%')
                ->orWhere('phone1', 'like', '%' . $search . '%')
                ->orWhere('q.nom', 'like', '%' . $search . '%')
                ->where('u.role_id', $role_id)->paginate(16);

        }
        else
            {
            $commerciaux = DB::table('users as u')->select('u.nom', 'u.prenom', 'u.adresse', 'phone1', 'q.nom as quartier', 'u.id', 'u.date_inscription')->orderBy('date_inscription', 'DESC')
                ->leftjoin('quartiers as q', 'q.id', '=', 'u.quartier_id')
                ->where('u.role_id', $role_id)->paginate(16);
        }
        return view('admin.commerciaux')->with(['clients' => $commerciaux])->with(['quartiers' => $quartiers]);
    }


    public function getAllboutiquiers()
    {
        $quartiers = Quartier::all();

        $role_id = Role::where('role', 'boutiquier')->get()->first()->id;

        $boutiquiers = DB::table('users as u')->select('u.nom', 'u.prenom', 'u.adresse', 'phone1', 'q.nom as quartier', 'u.id')->orderBy('date_inscription', 'DESC')
            ->leftjoin('quartiers as q', 'q.id', '=', 'u.quartier_id')
            ->where('u.role_id', $role_id)->paginate(16);
        return view('admin.boutiquiers')->with(['boutiquiers' => $boutiquiers])->with(['quartiers' => $quartiers]);
    }

    public function destroy($type, $user_id)
    {
        if($type == "client") {
            Client::where('id', (int)$user_id)->delete();
        }
        else
        {
            User::where('id', (int)$user_id)->delete();
        }
        return Redirect::back();
    }

    public function changePassword(Request $request)
    {
//        $older_password = $_POST['older-password'];
//
//        $test = User::where('password', bcrypt($older_password))->get();
//
//        return response()->json(bcrypt($older_password));


        $validatedData = $request->validate([
            'oldpass' => 'required|min:6',
            'password' => 'required|string|min:6',
            'password_confirmation' => 'required|same:password',
        ],[
            'oldpass.required' => 'Old password is required',
            'oldpass.min' => 'Old password needs to have at least 6 characters',
            'password.required' => 'Password is required',
            'password.min' => 'Password needs to have at least 6 characters',
            'password_confirmation.required' => 'Passwords do not match'
        ]);

        if($validatedData)
        {

            return $validatedData;
        }

        $current_password = \Auth::User()->password;
        if(\Hash::check($request->get('old-password'), $current_password))
        {
            $user_id = \Auth::User()->id;
            $obj_user = User::find($user_id);
            $obj_user->password = \Hash::make($request->get('old-password'));
            $obj_user->save();
            return view('auth.passwords.changeConfirmation');
        }
        else
        {
            $data['errorMessage'] = 'Please enter correct current password';
            return $data;
        }

    }

}
