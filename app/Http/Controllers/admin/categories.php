<?php

namespace App\Http\Controllers\admin;

use App\Categorie;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class categories extends Controller
{
    public function index()
    {
        $categories = DB::table('categories')->orderBy('orderBy', 'ASC')
            ->paginate(8);
        ;
        return view('admin.categories.index')->with(['categories' => $categories]);
    }

    public function delete($categorie_id)
    {
        Categorie::find($categorie_id)->delete();
        return  redirect('admin/categories');
    }

    public function show($categorie_id)
    {
        $categorie = Categorie::find($categorie_id);

        return view('admin.categories.show')->with(['categorie' => $categorie]);
    }

    public function edit($categorie_id)
    {
        $categorie = Categorie::find($categorie_id);

        $visibles = [ ['index' => true , 'nom' => 'Visible'], ['index' => false , 'nom' => 'Non visible'] ];

        return view('admin.categories.edit')->with(['categorie' => $categorie])->with(['visibles' => $visibles]);
    }

    public function update(Request $request)
    {
        $categorie = Categorie::find($request->get('categorie_id'));
        $visible = $request->get('visible');
        $libelle = $request->get('nom_categorie');


        $file_name = 'new_icone';
        $file = $request->file($file_name);

        if($file != null) {
            $newFileName = uniqid() . '.' . $file->clientExtension();

            $extension = \File::extension($file_name);

            $file->move(public_path() . '/images/icones', $newFileName);

            $icone = 'images/icones/' . $newFileName;
            $categorie->icone = $icone;
        }

        $categorie->libelle = $libelle;

        if( $visible != "0" && $visible == "1")
        {
            $categorie->visible = (bool)$visible;
        }

        if( $visible != "0" && $visible == null)
        {
            $categorie->visible = false;
        }

        $categorie->save();

        return  redirect('admin/categories');
    }

    public function create()
    {
        $visibles = [ ['index' => true , 'nom' => 'Visible'], ['index' => false , 'nom' => 'Non visible'] ];

        return view('admin.categories.new')->with(['visibles' => $visibles]);
    }

    public function store(Request $request)
    {

        $categorie = new Categorie();

        $visible = $request->get('visible');
        $libelle = $request->get('nom_categorie');


        $file_name = 'new_icone';
        $file = $request->file($file_name);

        $newFileName = uniqid() . '.' . $file->clientExtension();

        $extension = \File::extension($file_name);

        $file->move(public_path() . '/images/icones', $newFileName);

        $icone = 'images/icones/' . $newFileName;
        $categorie->icone = $icone;


        $categorie->libelle = $libelle;

        if($visible == "1")
        {
            $categorie->visible = (bool)$visible;
        }

        if($visible == null)
        {
            $categorie->visible = false;
        }

        $categorie->save();

        return  redirect('admin/categories');
    }

}
