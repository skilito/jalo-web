<?php

namespace App\Http\Controllers\Api;

use App\User;
use Illuminate\Http\Request;
use  App\Client;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Carbon\Carbon;
use App\Quartier;
use App\Notification;

class ClientController extends Controller
{
    /**
     * @SWG\Get(
     *   path="boutiquier/{id}/clients",
     *   summary="get customers by shopper",
     *   operationId="index",
     *   tags={"customers"},
     *   @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     description="id by shopper",
     *     required=true,
     *     type="integer"
     *   ),
     * @SWG\Parameter(
     *   name="authorization",
     *   type="string",
     *   required=true,
     *   in="header",
     *   description="add token"
     * ),
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=400, description="Token is required"),
     *   @SWG\Response(response=500, description="internal server error")
     * )
     *
     */
    public function index($boutiquier_id)
    {
        $clients = Client::getClientsByBoutiquier($boutiquier_id);

        return response()->json(['clients' => $clients],200);
    }

    /**
     * @SWG\Post(
     *   path="boutiquier/client/add",
     *   summary="create new customers by shopper",
     *   operationId="createCostumer",
     *   tags={"customers"},
     *   @SWG\Parameter(
     *     name="phone1",
     *     in="query",
     *     description="phone1 by new customer",
     *     required=true,
     *     type="string"
     *   ),
     * @SWG\Parameter(
     *     name="prenom",
     *     in="query",
     *     description="prenom by new customer",
     *     required=true,
     *     type="string"
     *   ),
     * @SWG\Parameter(
     *     name="nom",
     *     in="query",
     *     description="nom by new customer",
     *     required=true,
     *     type="string"
     *   ),
     * @SWG\Parameter(
     *   name="authorization",
     *   type="string",
     *   required=true,
     *   in="header",
     *   description="add token"
     * ),
     *   @SWG\Response(response=200, description="Costumer is created"),
     *   @SWG\Response(response=400, description="Token is required"),
     *   @SWG\Response(response=500, description="internal server error"),
     *   @SWG\Response(response=403, description="Ce quartier n'existe pas")
     * )
     *
     */
    public function createCostumer(Request $request)
    {
        $array = $request->all();

        $user = [
            "phone1" => $request->get('phone1'),
            "nom" => $request->get('nom'),
            "prenom" => $request->get('prenom'),
        ];

        $rules = array(
            'phone1'   => 'required|min:9',
        );
        $validator = Validator::make($user, $rules);

        if ($validator->fails()) {
            return response()->json(['error' =>$validator->errors()],403);
        }
        else
        {
            $user['date_creation'] = Carbon::now();
            $boutiquier = JWTAuth::toUser($request->header('Authorization'));
            $user['boutiquier_id'] = $boutiquier->id;
            $user['quartier_id'] = $boutiquier->quartier_id;

            $quartier = Quartier::find($user['quartier_id']);

            if($quartier == null){
                return response()->json(['message' => " Ce quartier n'existe pas"], 403);
            }

            if(User::verifyUser($user) == true)
            {
                Notification::sendNotif("new_number", $user, $boutiquier, null);

                $userCreated = Client::where('phone1', $user['phone1'])->get()->first();
                return response(['data' => $userCreated, 'status' => 200]);
            }
            else
            {
                if (Client::create($user)) {
                    if ($request->get('notif') == true) {
                        Notification::sendNotif("new_number", $user, $boutiquier, null);
                    }
                    $userCreated = Client::where('phone1', $user['phone1'])->get()->first();
                    return response(['data' => $userCreated, 'status' => 200]);
                } else
                {
                    return response()->json(['message' => 'Costumer not be created'], 403);
                }
            }

        }
    }

    /**
     * @SWG\Patch(
     *   path="boutiquier/client",
     *   summary="update customers by shopper",
     *   operationId="update",
     *   tags={"customers"},
     *   @SWG\Parameter(
     *     name="phone1",
     *     in="query",
     *     description="phone1 by new customer",
     *     required=true,
     *     type="string"
     *   ),
     * @SWG\Parameter(
     *     name="prenom",
     *     in="query",
     *     description="prenom by new customer",
     *     required=true,
     *     type="string"
     *   ),
     * @SWG\Parameter(
     *     name="nom",
     *     in="query",
     *     description="nom by new customer",
     *     required=true,
     *     type="string"
     *   ),
     * @SWG\Parameter(
     *     name="phone2",
     *     in="query",
     *     description="phone1 by new customer",
     *     type="string"
     *   ),
     * @SWG\Parameter(
     *     name="adresse",
     *     in="query",
     *     description="nom by new customer",
     *     type="string"
     * ),
     * @SWG\Parameter(
     *   name="authorization",
     *   type="string",
     *   required=true,
     *   in="header",
     *   description="add token"
     * ),
     *   @SWG\Response(response=200, description="update success"),
     *   @SWG\Response(response=400, description="Token is required"),
     *   @SWG\Response(response=500, description="internal server error"),
     *   @SWG\Response(response=403, description="this costumer doesn't exist"),
     *   @SWG\Response(response=404, description="attribute are missing")
     * )
     *
     */
    public function update(Request $request)
    {
        $client_id = $request->get('client_id');
        $rules = array(
            'nom'       => 'required|min:2',
            'prenom'     => 'required|min:2',
            'quartier_id'   => 'required',
            'phone1'   => 'required|min:9',
            'phone2'   => 'min:9',
            'adresse'  => 'min:3',
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json(['error' =>$validator->errors(), 'status' =>404]);
        } else {

            $user = Client::find($client_id);

            if($user == null)
            {
                return response()->json(['message' =>"this costumer doesn't exist", 'status' =>403]);
            }

            $user->nom = $request->get('nom');
            $user->prenom  = $request->get('prenom');
            $user->adresse  = $request->get('adresse');
            $user->phone1  = $request->get('phone1');
            $user->phone2  = $request->get('phone2');
            $user->quartier_id = $request->get('quartier_id');


            if($user->save())
            {
                return response()->json(['message' =>'update success', 'status' => 200]);
            }
            else
            {
                return response()->json(['message' =>'update not success', 'status' => 404]);
            }
        }
    }
    /**
     * @SWG\Get(
     *   path="commercial/clients",
     *   summary="get customers to commercial",
     *   operationId="getClientsByCommercial",
     *   tags={"customers"},
     * @SWG\Parameter(
     *   name="authorization",
     *   type="string",
     *   required=true,
     *   in="header",
     *   description="add token"
     * ),
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=400, description="Token is required"),
     *   @SWG\Response(response=500, description="internal server error")
     * )
     *
     */
    public function getClientsByCommercial(Request $request)
    {
        $commercial = JWTAuth::toUser($request->header('Authorization'));


    }

    public function  getClient($telephone)
    {
        $client = Client::where('phone1', '=', $telephone)->get();

        if($client != []) {
            return response()->json(['data' => $client ,'message' => "success"], 200);
        }
        else
        {
            return response()->json(['data' => $client, 'message' => "Ce client n'existe pas"], 200);
        }
    }

}
