<?php

namespace App\Http\Middleware;

use Closure;
use JWTAuth;
use Exception;
use Tymon\JWTAuth\Exceptions\JWTException;

class VerifyJWTToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try
        {
             JWTAuth::toUser($request->header('Authorization'));

        } catch (Exception $e)
        {
            if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException)
            {
                return response()->json(['error' => 'Token is Invalid'], 400);
            } else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException)
            {
                return response()->json(['error' => 'Token is Expired'],400);
            } else if ($e instanceof JWTException)
            {
                return response()->json(['error' => 'Token is required'],400);
            } else
            {
                return response()->json(['error' => 'Something is wrong'],400);
            }

        }
        return $next($request);
    }
}
