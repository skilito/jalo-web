<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MaBoutique extends Model
{
    protected $table = 'ma_boutique';
    protected $fillable = ['id','boutiquier_id', 'client_id'];
    public $timestamps = false;
}
