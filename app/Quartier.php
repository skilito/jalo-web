<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quartier extends Model
{
    protected $table = 'quartiers';
    protected $fillable = ['id','nom'];
    public $timestamps = false;

    public static function getQuartier()
    {
        return self::all();
    }

    public static function getQuartierById($quartier_id)
    {
      return self::find($quartier_id)->nom;
    }
}
