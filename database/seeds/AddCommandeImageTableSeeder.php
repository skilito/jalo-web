<?php

use Illuminate\Database\Seeder;
use App\Marchandise;

class AddCommandeImageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = $this->getFaker();

        $marchandises =Marchandise::all();

        foreach ($marchandises as $marchandise)
        {
            $marchandise->image = $faker->imageUrl($width = 640, $height = 480);
            $marchandise->save();
        }
    }

    public function getFaker()
    {
        if (empty($this->faker))
        {
            $faker = Faker\Factory::create();
            $faker->addProvider(new Faker\Provider\Base($faker));
            $faker->addProvider(new Faker\Provider\Lorem($faker));
        }
        return $this->faker = $faker;
    }
}
