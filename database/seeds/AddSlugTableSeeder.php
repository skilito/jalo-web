<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Utilities\Utility;

class AddSlugTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::all();
        foreach ($users as $user){
            $user->slug = Utility::generateSlug($user['prenom'], 'users');
            $user->save();
        }
    }
}
