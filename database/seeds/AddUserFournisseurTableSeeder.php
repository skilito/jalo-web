<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Info;
use App\Role;

class AddUserFournisseurTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = $this->getFaker();
        User::create([
            'nom' => $faker->lastName,
            'prenom' => $faker->firstName,
            'adresse' => $faker->streetAddress,
            'email'  => $faker->email,
            'sexe' => $faker->randomElement(['M', 'F']),
            'role_id' => 1,
            'info_id' => $faker->numberBetween(Info::all()->first()->id, Info::all()->last()->id),
            'phone1' => $faker->phoneNumber,
            'phone2' => $faker->phoneNumber,
            'date_naissance' => $faker->dateTime,
            'date_inscription' => $faker->dateTime,
            'password' => $faker->password(6,20),
            'avatar' => $faker->image(),
            'quartier_id' => 1,
            'token' => "837806a7-6c37-4630-9f6c-9aa7ad0129ed"

        ]);
    }

    public function getFaker()
    {
        if (empty($this->faker))
        {
            $faker = Faker\Factory::create();
            $faker->addProvider(new Faker\Provider\Base($faker));
            $faker->addProvider(new Faker\Provider\Lorem($faker));
        }
        return $this->faker = $faker;
    }
}
