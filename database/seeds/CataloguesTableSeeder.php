<?php

use Illuminate\Database\Seeder;
use App\Produit;
use App\User;
use App\Catalogue;

class CataloguesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faker = $this->getFaker();
        $four_id = \App\Role::where('role', 'fournisseur')->first()->id;
        $commer_id = \App\Role::where('role', 'commercial')->first()->id;

        for ($i = 0; $i < 100; $i++)
        {

            Catalogue::create([
                'produit_id' =>  $faker->numberBetween(Produit::all()->first()->id, Produit::all()->last()->id),
                'prix' => $faker->numberBetween($min = 100, $max = 10000),
                'fournisseur_id' => $faker->randomElement([User::where('role_id',$four_id)->first()->id, User::where('role_id',$four_id)->get()->last()->id]) ,
                'quantite' => $faker->numberBetween($min = 1, $max = 900),
                'date_ajout'=> $faker->dateTime,
                'photo' => $faker->image(),
                'date_fin_validite' => $faker->dateTime,
                'commercial_id' => $faker->randomElement([User::where('role_id',$commer_id)->first()->id, User::where('role_id',$commer_id)->get()->last()->id]),
                'valide' => (bool)random_int(0, 1)

            ]);
        }
    }
    public function getFaker()
    {
        if (empty($this->faker))
        {
            $faker = Faker\Factory::create();
            $faker->addProvider(new Faker\Provider\Base($faker));
            $faker->addProvider(new Faker\Provider\Lorem($faker));
        }
        return $this->faker = $faker;
    }
}
