<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Client;
use App\Quartier;

class CientsTableSedeer extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = $this->getFaker();

        $role = \App\Role::where('role','boutiquier')->first()->id;

        $user = \App\User::where('role_id', $role)->first()->id;

//        $array = [];
//
//        foreach ($users as $user){
//            array_push($array, $user->id);
//        }
//        dd($array);

        for ($i = 0; $i < 10; $i++)
        {

            Client::create([
                'nom' => $faker->lastName,
                'prenom' => $faker->firstName,
                'adresse' => $faker->streetAddress,
                'phone1' => $faker->phoneNumber,
                'phone2' => $faker->phoneNumber,
                'date_creation' => $faker->dateTime,
                'quartier_id' => $faker->numberBetween(Quartier::all()->first()->id, Quartier::all()->last()->id),
                'boutiquier_id' => $user
            ]);
        }
    }

    public function getFaker()
    {
        if (empty($this->faker))
        {
            $faker = Faker\Factory::create();
            $faker->addProvider(new Faker\Provider\Base($faker));
            $faker->addProvider(new Faker\Provider\Lorem($faker));
        }
        return $this->faker = $faker;
    }
}
