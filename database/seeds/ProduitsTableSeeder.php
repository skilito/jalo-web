<?php

use Illuminate\Database\Seeder;
use App\Categorie;
use App\Produit;

class ProduitsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faker = $this->getFaker();

        for ($i = 0; $i < 100; $i++)
        {

            Produit::create([
                'libelle' => $faker->word,
                'categorie_id' => $faker->numberBetween(Categorie::all()->first()->id, Categorie::all()->last()->id),

            ]);
        }
    }

    public function getFaker()
    {
        if (empty($this->faker))
        {
            $faker = Faker\Factory::create();
            $faker->addProvider(new Faker\Provider\Base($faker));
            $faker->addProvider(new Faker\Provider\Lorem($faker));
        }
        return $this->faker = $faker;
    }
}
