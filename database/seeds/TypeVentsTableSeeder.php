<?php

use Illuminate\Database\Seeder;
use App\TypeVente;

class TypeVentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = ["grossiste","detail"];

        foreach ($types as $type){
            TypeVente::create([
                'libelle' => $type
            ]);
        }
    }
}
