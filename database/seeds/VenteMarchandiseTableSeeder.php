<?php

use Illuminate\Database\Seeder;
use App\VenteMarchandise;
use App\Marchandise;
use App\Vente;

class VenteMarchandiseTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = $this->getFaker();

        for ($i = 0; $i < 50; $i++)
        {

            VenteMarchandise::create([
                'marchandise_id' => $faker->numberBetween(Marchandise::all()->first()->id, Marchandise::all()->last()->id),
                'vente_id' => $faker->numberBetween(Vente::all()->first()->id, Vente::all()->last()->id),
                'quantite' => $faker->numberBetween($min = 1, $max = 900)
            ]);
        }
    }

    public function getFaker()
    {
        if (empty($this->faker))
        {
            $faker = Faker\Factory::create();
            $faker->addProvider(new Faker\Provider\Base($faker));
            $faker->addProvider(new Faker\Provider\Lorem($faker));
        }
        return $this->faker = $faker;
    }
}
