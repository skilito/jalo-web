jQuery(document).ready(function($) {
	$(document).foundation();

	// Materialze css
	$('select').material_select();

  //Tool tip
  $('.tooltipped').tooltip({delay: 50});

  //Add to bucket
  $('.add-bucket-btn .material-icons').click(function(e) {
    $(this).html('remove_shopping_cart');
  });

  $('select').material_select('destroy');

    //Modal initializer
    $(`#modal1,
   #add-admin,
   #facture-vente,
   #create-user,
   #edit-user,
   #create-commande,
   #create-shop,
   #show-commande,
   #bucket-container,
   #new-client,
   #new-seller,
   #new-dealer,
   #show-item-seller,
   #show-item-commande,
   #add-product,
   #edit-product,
   #edit-seller,
   #edit-client,
   #new-shopper,
   #edit-item-client,
   #add-provider,
   #show-item-client,
   #new-client,
   #new-boutique,
   #show-shopper,
   #edit-fournisseur,
   #change-password  
   `).modal();

	//Add more product
	$('#add-more').click(function() {
		$('#product-list').append(`
			<div class="row product-list-item">
        <div class="small-12 medium-12 large-6 p-r-10">
          <div class="input-field">
            <input id="last_name" type="text" class="validate">
            <label for="last_name"  data-error="ce produit n'existe pas" >Nom produit</label>
          </div>
        </div>
        <div class="small-12 medium-12 large-2 p-r-10">
          <div class="input-field">
            <input id="last_name" type="number" class="validate">
            <label for="last_name"  data-error="ce champ ne prend que des chiffres" >Quantité</label>
          </div>
        </div>
        <div class="small-12 medium-12 large-2 p-r-10">
          <div class="input-field">
            <input id="last_name" type="number" class="validate">
            <label for="last_name"  data-error="ce champ ne prend que des chiffres" >Prix unitailre</label>
          </div>
        </div>
        <div class="small-12 medium-12 large-2 p-r-10 tar">
          <a class="btn-floating btn-tiny waves-effect waves-black red" id="remove-this-item"><i class="material-icons">close</i></a>
        </div>
      </div>
		`);
		$('#remove-this-item').click(function() {
			$('.product-list-item').remove();
      console.log('Item supprimé');
		});
	});



	// $('#add-more-commande-client').click(function() {
	// 	$('#commande-list').append(`
	// 		<div class="row commande-list-item">
     //    <div class="small-12 medium-12 large-4 p-r-10">
     //      <div class="input-field">
     //        <input id="last_name" type="text" class="validate">
     //        <label for="last_name"  data-error="ce produit n'existe pas" >Nom produit</label>
     //      </div>
     //    </div>
     //    <div class="small-12 medium-12 large-2 p-r-10">
     //      <div class="input-field">
     //        <input id="last_name" type="number" class="validate">
     //        <label for="last_name"  data-error="ce champ ne prend que des chiffres" >Quantité</label>
     //      </div>
     //    </div>
     //    <div class="small-12 medium-12 large-2 p-r-10">
     //      <div class="input-field">
     //        <input id="last_name" type="number" class="validate" value="0" disabled>
     //      </div>
     //    </div>
     //    <div class="small-12 medium-12 large-2 p-r-10">
     //      <div class="input-field">
     //        <input id="last_name" type="number" class="validate" value="0" disabled>
     //      </div>
     //    </div>
     //    <div class="small-12 medium-12 large-2 p-r-10 tar">
     //      <button class="btn-floating btn-tiny waves-effect waves-black red" id="remove-this-item"><i class="material-icons">close</i></button>
     //    </div>
     //  </div>
	// 	`);
	// 	$('#remove-this-item').click(function() {
	// 		// $(this).parents().remove();
     //  console.log('Item supprimé');
	// 	});
	// });
});