@extends('layouts.admin.master-admin')
@section('title')
    JALÔ - Tableau de bord
@endsection
@section('app-css')
    <link rel="stylesheet" href="../css/app.css">
@endsection
@section('content')
    <main class="main">
        <p>&nbsp;</p>

        <form action="boutiques" method="get">
            <div class="row">
                <div class="small-12 medium-6 large-4 p-10">
                    <label for="">
                        Quartier
                        <select name="quartier">
                            <option value="" disabled selected>Sélectioner votre quartier</option>
                            @foreach($quartiers as $quartier)
                                <option value="{{$quartier->nom}}">{{$quartier->nom}}</option>
                            @endforeach
                        </select>
                    </label>
                </div>
                <div class="small-12 medium-6 large-3 p-10">
                    <label for="">
                        Date début
                        <input type="date" name="dateDebut">
                    </label>
                </div>
                <div class="small-12 medium-6 large-3 p-10">
                    <label for="">
                        Date Fin
                        <input type="date" name="dateFin">
                    </label>
                </div>
                <div class="small-12 medium-6 large-2 p-10 filter">
                    <button class="button expanded" style="margin-bottom: 0;">Filter</button>
                </div>
            </div>
        </form>
        <p>&nbsp;</p>
        <!-- Liste des commandes -->
        <div class="row">
            <div class="small-12 medium-12 large-12">
                <table class="unstriped">
                    <thead>
                    <tr>
                        {{--<th>Produits</th>--}}
                        <th>Commercial</th>
                        <th>Téléphone Commercial</th>
                        <th>Boutiquier</th>
                        <th>Téléphone Boutiquier</th>
                        <th>Quartier</th>
                        <th>Date inscription</th>
                        {{--<th>Action</th>--}}
                    </tr>
                    </thead>
                    <tbody>
                    @if($boutiques != [])
                        @foreach($boutiques as $boutique)
                            <tr>

                                <td>{{ ucfirst($boutique->prenomCommercial) }} {{ ucfirst($boutique->nomCommercial)}}</td>
                                <td>{{$boutique->telCommercial}}</td>
                                <td>{{ ucfirst($boutique->prenomBoutiquier) }} {{ ucfirst($boutique->nomBoutiquier)}}</td>
                                <td>{{ $boutique->telBoutiquier }}</td>
                                <td>{{ $boutique->quartier }}</td>
                                <td>{{ $boutique->date_inscription }}</td>
                            </tr>
                        @endforeach
                    @else
                        <h1>Pas de boutiques</h1>
                    @endif
                    </tbody>
                </table>
                <div class="row">
                    <div class="small-12 medium-12 large-12">
                        <ul class="pagination text-center" role="navigation" aria-label="Pagination">
                            {{ $boutiques->appends(request()->query())->links() }}
                        </ul>
                    </div>
                </div>
            </div>
        </div>

    </main>
@section('script')
    <script src="../../js/vendors/jquery.min.js"></script>
    <script src="../../js/vendors/foundation.min.js"></script>
    <script src="../../js/vendors/materialize.min.js"></script>
    <script src="../../js/app.js"></script>
@endsection
@endsection