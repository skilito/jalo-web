@extends('layouts.admin.master-admin')
@section('title')
    JALÔ - Tableau de bord
@endsection
@section('app-css')
    <link rel="stylesheet" href="/../css/app.css">
@endsection
@section('content')

    <main class="main">
        <p>&nbsp;</p>

        <div class="row">
            <div class="small-12 medium-8 medium-offset-2 large-6 large-offset-3">
                <header class="header-modal">
                    <h4>Edit Catégorie</h4>
                </header>
                <p>&nbsp;</p>
                <form action="/admin/categorie/save" id="saveCategorie" method="post" enctype="multipart/form-data">
                    <div class="row">
                        <div class="small-12 medium-12 large-12">
                            <label for="editNom" data-error="...">Nom Catégorie *</label>
                            <div class="input-field">
                                <input id="nom_categorie" type="text" class="validate" name="nom_categorie" required>
                            </div>
                        </div>

                        {{ csrf_field() }}
                        <div class="small-12 medium-12 large-12">
                            <label for="editFournisseur" data-error="...">Visibilité *</label>
                            <div class="input-field-custom">
                                <select id="visible" name="visible" required>
                                    @foreach($visibles as $visible)
                                        <option value="{{ $visible['index'] }}">{{ucfirst($visible['nom'])}} </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="small-12 medium-12 large-12">
                            <div class="file-field input-field">
                                <span>Icone</span>
                                <input type="file" name="new_icone" required>
                            </div>
                        </div>

                    </div>
                </form>
                <div class="form-footer">
                    <a href="/admin/categories" class="modal-action modal-close waves-effect waves-black btn-flat success button">ANNULER</a>
                    <button href="#!" class="modal-action waves-effect waves-black btn-flat grey button"
                            form="saveCategorie" type="submit" data-disable-with="<i class='fa fa-spinner fa-spin'></i> loading.....">Enregistrer
                    </button>
                </div>
            </div>
        </div>

    </main>

@section('script')
    <script src="/../js/vendors/jquery.min.js"></script>
    <script src="/../js/vendors/foundation.min.js"></script>
    <script src="/../js/vendors/materialize.min.js"></script>
    <script src="/../js/app.js"></script>
    <script src="/../js/vendors/flickity.pkgd.min.js"></script>
@endsection

@endsection



