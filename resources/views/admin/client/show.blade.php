@extends('layouts.admin.master-admin')
@section('title')
    show
@endsection
@section('app-css')
    <link rel="stylesheet" href="../../../css/app.css">
@endsection
@section('content')

    <main class="main">
        <p>&nbsp;</p>
        <div class="row">
            <div class="small-12 medium-8 medium-offset-2 large-6 large-offset-3">
                <header class="header-modal">
                    <h4>Détails Client </h4>
                </header>
                <p>&nbsp;</p>
                <form action="">
                    <div class="row">

                        <div class="small-12 medium-12 large-12">
                            <div class="input-field">
                                <input id="last_name" type="text" class="validate" value="{{ ucfirst($client->prenom)}}" disabled="">
                                <label for="last_name" class="active" data-error="..." >Prénom *</label>
                            </div>
                        </div>
                        <div class="small-12 medium-12 large-12">
                            <div class="input-field">
                                <input id="last_name" type="text" class="validate" value="{{ucfirst($client->nom)}}" disabled="">
                                <label for="last_name" class="active" data-error="..." >Nom *</label>
                            </div>
                        </div>
                        <div class="small-12 medium-12 large-12">
                            <div class="input-field">
                                <input id="last_name" type="tel" class="validate" value="@if($client->phone1)  {{$client->phone1}} @else no reseigné @endif" disabled="">
                                <label for="last_name" class="active"  data-error="..." >Téléphone *</label>
                            </div>
                        </div>
                        {{--<div class="small-12 medium-12 large-12">--}}
                            {{--<div class="input-field">--}}
                                {{--<input id="last_name" type="email" class="validate" value="@if($client->email)  {{$client->email}} @else No rensegné @endif" disabled="">--}}
                                {{--<label for="last_name" class="active"  data-error="..." >Email *</label>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        <div class="small-12 medium-12 large-12">
                            <div class="input-field">
                                <input id="last_name" type="text" class="validate" value="@if($client->adresse)  {{$client->adresse}} @else No renseigné @endif" disabled="">
                                <label for="last_name" class="active"  data-error="..." >Adresse </label>
                            </div>
                        </div>
                    </div>
                </form>
                <p>&nbsp;</p>
                {{--<hr>--}}
                <div class="form-footer">
                    <a href="/admin/client/{{$client->id}}/edit" class="modal-action modal-close waves-effect waves-black btn-flat success button">MODIFIER</a>
                    <a href="/admin/clients" class="modal-action modal-close waves-effect waves-black btn-flat grey button">ANNULER</a>
                </div>
            </div>
        </div>

    </main>


@section('script')
    <script src="../../../js/vendors/jquery.min.js"></script>
    <script src="../../../js/vendors/foundation.min.js"></script>
    <script src="../../../js/vendors/materialize.min.js"></script>
    <script src="../../../js/app.js"></script>
@endsection

@endsection
