@extends('layouts.admin.master-admin')
@section('title')
    JALÔ - Tableau de bord
@endsection
@section('app-css')
    <link rel="stylesheet" href="../../../css/app.css">
@endsection
@section('content')

    <main class="main">
        <p>&nbsp;</p>

        <div class="row">
            <div class="small-12 medium-8 medium-offset-2 large-6 large-offset-3">
                <header class="header-modal">
                    <h4>Edit Produit</h4>
                </header>
                <p>&nbsp;</p>
                <form action="/product/update" id="saveEditProduct" method="post" enctype="multipart/form-data">
                    <input type="hidden" value="{{$catalogue->catalogue_id}}" name="catalogue_id">
                    <div class="row">
                        <div class="small-12 medium-12 large-12">
                            <label for="editNom" data-error="...">Nom Produit *</label>
                            <div class="input-field">
                                <input id="editNom" type="text" class="validate" name="editNom"
                                       value="{{$catalogue->nomProduit}}">
                            </div>
                        </div>
                        <div class="small-12 medium-12 large-12">
                            <label for="editDescription" data-error="...">Description Produit *</label>
                            <div class="input-field">
                                <input id="editDescription" type="text" class="validate" name="editDescription"
                                       value="{{$catalogue->description}}">
                            </div>
                        </div>
                        <div class="small-12 medium-12 large-12">
                            <label for="editPrice" data-error="...">Prix *</label>
                            <div class="input-field">
                                <input id="editPrice" type="number" class="validate" name="editPrice"
                                       value="{{$catalogue->prix}}">
                            </div>
                        </div>
                        {{ csrf_field() }}
                        <div class="small-12 medium-12 large-12">
                            <label for="editFournisseur" data-error="...">Fournisseur *</label>
                            <div class="input-field-custom">
                                <select id="fournisseur-edit" name="editFournisseur">
                                    <option value="0">{{$catalogue->fournisseur_prenom}} {{$catalogue->fournisseur_nom}}</option>
                                    @foreach($fournisseurs as $fournisseur)
                                        <option value="{{$fournisseur->id}}">{{ucfirst($fournisseur->prenom)}} {{ucfirst($fournisseur->nom)}} </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="small-12 medium-12 large-12">
                            <label for="editFournisseur" data-error="...">Catégorie </label>
                            <div class="input-field-custom">
                                <select id="categorie-edit" name="editCategorie">
                                    <option value="0">{{ucfirst($catalogue->categorie)}}</option>
                                    @foreach($categories as $categorie)
                                        <option value="{{$categorie->id}}">{{ucfirst($categorie->libelle)}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="small-12 medium-12 large-12" style="margin-top: 20px">
                            <label for="EditPrixPromo" data-error="...">Prix promo</label>
                            <div class="input-field">
                                <input id="EditPrixPromo" type="number" class="validate" name="EditPrixPromo"
                                       value="{{$catalogue->promo_prix}}">
                            </div>
                        </div>
                        {{--<p>&nbsp;</p>--}}

                        <div class="small-12 medium-12 large-12">
                            <div class="row no-margin">
                                <div class="small-12 medium-12 large-5">
                                    <label for="EditPourcentageJalo" data-error="...">Pourcentage JALO</label>
                                    <div class="input-field">
                                        <input id="EditPourcentageJalo" type="number" class="validate"
                                               name="EditPourcentageJalo" value="{{$catalogue->pourcentage_jalo}}">
                                    </div>
                                </div>
                                <div class="small-12 medium-12 large-5 large-offset-2">
                                    <label for="EditPourcentageBoutiquier" data-error="...">Pourcentage
                                        Boutiquier</label>
                                    <div class="input-field">
                                        <input id="EditPourcentageBoutiquier" type="number" class="validate"
                                               name="EditPourcentageBoutiquier"
                                               value="{{$catalogue->pourcentage_boutiquier}}">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="small-12 medium-12 large-12">
                            <div class="file-field input-field">
                                <span>Photo Produit</span>
                                <input type="file" name="new_photo">

                            </div>
                        </div>

                    </div>
                </form>
                <div class="form-footer">
                    <a href="#!" class="modal-action modal-close waves-effect waves-black btn-flat success button">ANNULER</a>
                    <button href="#!" class="modal-action waves-effect waves-black btn-flat grey button"
                            form="saveEditProduct" type="submit" data-disable-with="<i class='fa fa-spinner fa-spin'></i> loading.....">Enregistrer
                    </button>
                </div>
            </div>
        </div>

    </main>

@section('script')
    <script src="../../../js/vendors/jquery.min.js"></script>
    <script src="../../../js/vendors/foundation.min.js"></script>
    <script src="../../../js/vendors/materialize.min.js"></script>
    <script src="../../../js/app.js"></script>
@endsection

@endsection



