@extends('layouts.app')

@section('content')
    <div class="sign-page">
         <div class="sign-page-left" style="overflow: hidden;">
            <div class="main-carousel">
                <div class="carousel-cell">
                    <img src="images/background-slider.png" alt="">
                    <div class="text-filter">
                        <h2 class="text-filter-title">Tous près de chez vous Slider - 1</h2>
                    </div>
                </div>
                <div class="carousel-cell">
                    <img src="images/background-slider.png" alt="">
                    <div class="text-filter">
                        <h2 class="text-filter-title">Tous près de chez vous Slider - 2</h2>
                    </div>
                </div>
                <div class="carousel-cell">
                    <img src="images/background-slider.png" alt="">
                    <div class="text-filter">
                        <h2 class="text-filter-title">Tous près de chez vous Slider - 3</h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="sign-page-right">
            <img src="images/logo-black.jpeg" alt="" class="logo">
            <header class="header-sign">
                <a href="/login" class="hollow button secondary">Se connecter</a>
            </header>
            <div class="form-sign-wrapper">
                <div class="form-sign">
                    <h4>Inscription</h4>
                    <form action="/register" method="post">
                        {{ csrf_field() }}
                        <span style="color: red">@if (session('message')) {{ session('message') }} @endif</span>
                        <div class="input-field">
                            <label for="phone1" data-error=@if($errors->has('phone1')) {{ $errors->first('phone1') }}@endif>Numéro de téléphone *</label>
                            <input id="phone1" type="tel" class="validate" name="phone1" required>
                        </div>
                        <div class="input-field">
                            <label for="last_name"  data-error="votre adresse eamil est invalide" >Adresse email</label>
                            <input id="last_name" type="email" class="validate" name="email">
                        </div>
                        <div class="input-field">
                            <label for="prenom">Prénom *</label>
                            <input id="prenom" type="text" class="validate" name="prenom" required>
                            <span style="color: red">@if($errors->has('prenom')) {{ $errors->first('prenom') }}@endif</span>
                        </div>
                        <div class="input-field">
                            <label for="nom">Nom *</label>
                            <input id="nom" type="text" class="validate" name="nom" required>
                            @if($errors->has('nom')) {{ $errors->first('nom') }}@endif
                        </div>
                        <div class="input-field">
                            <select name="quartier" required>
                                <option value="" disabled selected>Sélectioner votre quartier</option>
                                @foreach($quartiers as $quartier)
                                    <option value="{{$quartier->id}}">{{$quartier->nom}}</option>
                                @endforeach
                                <span style="color: red">@if($errors->has('quartier')) {{ $errors->first('quartier') }}@endif</span>
                            </select>
                        </div>
                        <div class="input-field">
                            <label for="password"  data-error="Votre mot de passe est incorect">Mot de passe *</label>
                            <input id="password" type="password" class="validate" name="password" required>
                            <span style="color: red">@if($errors->has('password')) {{ $errors->first('password') }}@endif</span>
                        </div>
                        <div class="input-field">
                            <label for="password_confirmation"  data-error="Votre mot de passe est incorect">Confirmer votre mot de passe *</label>
                            <input id="password" type="password" class="validate" name="password_confirmation" required>
                        </div>
                        <div class="input-field">
                            <select name="info_id" required>
                                <option value="" disabled selected>Comment vous avez connu Jalo</option>
                                @foreach($infos as $info)
                                    <option value="{{$info->id}}">{{$info->label}}</option>
                                @endforeach
                                <span style="color: red">@if($errors->has('info_id')) {{ $errors->first('info_id') }}@endif</span>
                            </select>
                        </div>
                        <p>&nbsp;</p>
                        <div class="footer-form-button">
                            <button class="button secondary" type="submit" value="Submit">S'inscrire</button>
                            <a href="/login" class="link-to-sign">J'ai déjà un compte</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
