<!doctype html>
<html class="no-js" lang="fr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Jalo</title>
    <link rel="stylesheet" href="../css/app.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>
<body>

<header class="header">
    @include('layouts.client.headertop_client')
    @include('layouts.client.menu')
</header>

<main class="main">

    <div class="row">
        <header class="header-main-content">
            <div class="top-bar">
                <div class="top-bar-left">
                    <ul class="menu">
                        <li><a href="catalogues?category=all" class="button">Toutes les catégories</a></li>
                        @if($categories != [])
                            @foreach($categories as $categorie)
                                <li><a href="catalogues?category={{$categorie->id}}"
                                       class="button">{{$categorie->libelle}}</a></li>
                            @endforeach
                        @endif
                    </ul>
                </div>
                <div class="top-bar-right pos-rltv">
                    <div class="fixed-action-btn horizontal">
                        <a class="waves-effect waves-yellow btn-floating btn-large black">
                            <i class="material-icons">add_shopping_cart</i>
                        </a>
                        <!-- Modal Trigger -->
                        <ul>
                            <li>
                                <a class="waves-effect waves-black btn-floating green darken-1 modal-trigger"
                                   href="#create-shop" data-position="bottom" data-delay="50"
                                   data-tooltip="Faire une commande">
                                    <i class="material-icons">add_shopping_cart</i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </header>
    </div>

    <div class="row">
        <header class="header-main-content">
            <div class="top-bar">
                <div class="top-bar-left">
                    <ul class="menu">
                        <li><a href="catalogues?category=all" class="button">Toutes les catégories</a></li>
                        @if($categories != [])
                            @foreach($categories as $categorie)
                                <li><a href="catalogues?category={{$categorie->id}}"
                                       class="button">{{$categorie->libelle}}</a></li>
                            @endforeach
                        @endif
                    </ul>
                </div>
                <div class="top-bar-right pos-rltv">
                    <div class="fixed-action-btn horizontal">
                        <a class="waves-effect waves-yellow btn-floating btn-large black">
                            <i class="material-icons">add_shopping_cart</i>
                        </a>
                        <!-- Modal Trigger -->
                        <ul>
                            <li>
                                <a class="waves-effect waves-black btn-floating green darken-1 modal-trigger"
                                   href="#create-shop" data-position="bottom" data-delay="50"
                                   data-tooltip="Faire une commande">
                                    <i class="material-icons">add_shopping_cart</i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </header>
    </div>
    <div class="row">
        @if($catalogues != [])
            @foreach($catalogues as $catalogue)
                <div class="small-12 medium-6 large-3 p-10">
                    <div class="card card-center">
                        <img src="https://source.unsplash.com/collection/190727" alt="">
                        <div class="card-section">
                            <h4 class="card-title">{{ $catalogue->productLibelle}}</h4>
                            <strong class="card-price">{{ $catalogue->prix}} <span>F cfa</span></strong>
                            <form class="form">
                                {{ csrf_field() }}
                                <input type="hidden" value="{{ $catalogue->id}}" name="catalogue_id"
                                       class="catalogue_id" readonly>
                                <input type="hidden" value="{{ Auth::user()->id}}" name="client_id" class="client_id"
                                       readonly>
                                <input type="hidden" value="{{ $catalogue->productLibelle}}" class="item-name client_id"
                                       readonly>
                                <input type="hidden" value="{{ $catalogue->prix }}" class="item-price client_id"
                                       readonly>

                                <button class="add-bucket-btn waves-effect waves-black" type="submit" value="Submit"
                                        id="add_panier">
                                    Ajouter au panierF
                                    <span class="btn-floating btn-tiny waves-effect waves-yellow white">
                  <i class="material-icons">add</i>
                  </span>
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            @endforeach
        @endif
    </div>

    <div class="row">
        <div class="small-12 medium-12 large-12">
            <ul class="pagination text-center" role="navigation" aria-label="Pagination">
                <!-- <li class="pagination-previous disabled">Précédent</li>
                <li class="current"><span class="show-for-sr">Page courante</span> 1</li>
                <li><a href="#" aria-label="Page 2">2</a></li>
                <li><a href="#" aria-label="Page 3">3</a></li>
                <li><a href="#" aria-label="Page 4">4</a></li>
                <li class="ellipsis"></li>
                <li><a href="#" aria-label="Page 12">12</a></li>
                <li><a href="#" aria-label="Page 13">13</a></li>
                <li class="pagination-next"><a href="#" aria-label="Next page">Suivant</a></li> -->

                @if((Request::only('category')!=null))
                    {{ $catalogues->appends(Request::only('category'))->links() }}
                @endif
            </ul>
        </div>
    </div>


    <!-- Modal Structure -->
    <div id="modal1" class="modal modal-fixed-footer">
        <div class="modal-content">
            <h4>Nouvelle vente</h4>
            <div class="row">
                <div class="small-12 medium-12 large-8 p-r-30">
                    <div class="input-field">
                        <input id="last_name" type="text" class="validate">
                        <label for="last_name" data-error="ce client n'existe pas">Nom du client</label>
                    </div>
                </div>
                <div class="small-12 medium-12 large-4">
                    <button class="add-bucket-btn waves-effect waves-black w-100 modal-trigger" href="#create-user">
                    Créer client <span class="btn-floating btn-tiny waves-effect waves-yellow white"><i
                                class="material-icons">person_add</i></span>
                    </button>
                </div>
            </div>
            <p>&nbsp;</p>
            <div class="row">
                <div class="small-12 medium-12 large-6"><p>Produits</p></div>
                <div class="small-12 medium-12 large-2"><p>Quantité</p></div>
                <div class="small-12 medium-12 large-2"><p>Prix unitaire</p></div>
                <div class="small-12 medium-12 large-2"></div>
            </div>

            <div id="product-list">
                <div class="row product-list-item">
                    <div class="small-12 medium-12 large-6 p-r-10">
                        <div class="input-field">
                            <input id="last_name" type="text" class="validate">
                            <label for="last_name" data-error="ce produit n'existe pas">Nom produit</label>
                        </div>
                    </div>
                    <div class="small-12 medium-12 large-2 p-r-10">
                        <div class="input-field">
                            <input id="last_name" type="number" class="validate">
                            <label for="last_name" data-error="ce champ ne prend que des chiffres">Quantité</label>
                        </div>
                    </div>
                    <div class="small-12 medium-12 large-2 p-r-10">
                        <div class="input-field">
                            <input id="last_name" type="number" class="validate">
                            <label for="last_name" data-error="ce champ ne prend que des chiffres">Prix
                                unitailre</label>
                        </div>
                    </div>
                    <div class="small-12 medium-12 large-2 p-r-10 tar">
                        <button class="btn-floating btn-tiny waves-effect waves-black red" id="remove-this-item"><i
                                    class="material-icons">close</i></button>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="small-12 medium-12 large-12 p-r-10 tar">
                    <a class="waves-effect waves-black hollow button secondary" id="add-more">Ajouter un produit</a>
                </div>
            </div>

        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-action modal-close waves-effect waves-black btn-flat grey button">ANNULER</a>
            <a class="modal-action modal-close waves-effect waves-black btn-flat success button modal-trigger"
               href="#facture-vente" type="submit" value="Submit" id="add-com">VALIDER</a>
        </div>
    </div>

    <div id="bucket-container" class="modal modal-fixed-footer">
        <div class="modal-content">
            <header class="header-modal">
                <h4>Mon panier</h4>
            </header>
            <p>&nbsp;</p>
            <div class="row">
                <div class="small-12 medium-12 large-4"><p>Produits</p></div>
                <div class="small-12 medium-12 large-2"><p>Quantité</p></div>
                <div class="small-12 medium-12 large-2"><p>Prix unitaire</p></div>
                <div class="small-12 medium-12 large-2 tar"><p>Tatal</p></div>
                <div class="small-12 medium-12 large-2 tar"><p>Annuler</p></div>
            </div>
            <div class="bucket-items">
                @include('client.panier')
            </div>
            <div class="total-paid">
                <div class="row">
                    <div class="small-12 medium-12 large-6 large-offset-4 tar">
                        <em>Total à payer</em>: <strong>1 600 <i>F cfa</i></strong>
                    </div>
                </div>
            </div>
            <p>&nbsp;</p>
            <div class="row">
                <div class="small-12 medium-12 large-6 large-offset-6">
                    <ul class="shop-info">
                        @if($boutiquier != [])
                            <li><p>Nom du Boutiquier</p><strong>{{$boutiquier->prenom}} {{$boutiquier->nom}} </strong>
                            </li>
                            <li><p>Quartier</p><strong>{{$boutiquier->quartier}}</strong></li>
                        @endif
                    </ul>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-action modal-close waves-effect waves-black btn-flat grey button">ANNULER
                VENTE</a>
            <a href="#!" id="order" class="waves-effect waves-black btn-flat success button" onclick="orderbasket()">Valider</a>
        </div>
    </div>

    <div id="create-user" class="modal modal-fixed-footer">
        <form action="">
            <div class="modal-content">
                <h4>Nouveau client</h4>
                <div class="row">
                    <div class="small-12 medium-12 large-8 large-offset-2">
                        <div class="input-field">
                            <input id="last_name" type="text" class="validate">
                            <label for="last_name" data-error="ce produit n'existe pas">Prénom *</label>
                        </div>
                    </div>
                    <div class="small-12 medium-12 large-8 large-offset-2">
                        <div class="input-field">
                            <input id="last_name" type="text" class="validate">
                            <label for="last_name" data-error="ce produit n'existe pas">Nom *</label>
                        </div>
                    </div>
                    <div class="small-12 medium-12 large-8 large-offset-2">
                        <div class="input-field">
                            <input id="last_name" type="tel" class="validate">
                            <label for="last_name" data-error="numéro email invalide">Téléphone *</label>
                        </div>
                    </div>
                    <div class="small-12 medium-12 large-8 large-offset-2">
                        <div class="input-field">
                            <input id="last_name" type="email" class="validate">
                            <label for="last_name" data-error="adresse email invalide">email *</label>
                        </div>
                    </div>
                    <div class="small-12 medium-12 large-8 large-offset-2">
                        <div class="input-field">
                            <select>
                                <option value="" disabled selected>Sélectioner son quartier</option>
                                <option value="1">Maristes</option>
                                <option value="2">Almadies</option>
                                <option value="3">Pikine</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <a href="#!" class="modal-action modal-close waves-effect waves-black btn-flat grey button">ANNULER</a>
                <a href="#!" class="modal-action modal-close waves-effect waves-black btn-flat success button">CRÉER
                    CLIENT</a>
            </div>
        </form>
    </div>

    <div id="create-shop" class="modal modal-fixed-footer" onmouseenter="createShop()">

        <div class="modal-content">        {{-- ajout nouvelle commande vddjdvvhvvdgvvvahdfhhhhghghgghghghghgjgjjj --}}
            <h4>Nouvelle commande</h4>
            <h5>Sélectioner les produits</h5>
            <p>&nbsp;</p>
            <div class="row">
                <div class="small-12 medium-12 large-4"><p>Produits</p></div>
                <div class="small-12 medium-12 large-2"><p>Quantité</p></div>
                <div class="small-12 medium-12 large-2"><p>Prix unitaire</p></div>
                <div class="small-12 medium-12 large-2"><p>Total</p></div>
                <div class="small-12 medium-12 large-2"></div>
            </div>

            <div id="commande-list">

                <div class="row commande-list-item">
                    <div class="small-12 medium-12 large-4 p-r-10">
                        <div class="input-field">
                            <select name="productName" onchange="addItem()" required>
                                <option value="" disabled selected>Sélectioner votre produit</option>
                                @foreach($products as $product)
                                    <option value="{{$product->catalogue_id}}">{{$product->libelle}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    {{ csrf_field() }}
                    <div class="small-12 medium-12 large-2 p-r-10">
                        <div class="input-field">
                            <input id="shop-qnt" oninput="calculTotalShop()" type="number" class="validate" required>
                            <label for="shop-qnt" data-error="ce champ ne prend que des chiffres">quantité</label>
                        </div>
                    </div>
                    <div class="small-12 medium-12 large-2 p-r-10">
                        <div class="input-field">
                            <input id="price" type="number" class="validate" disabled>
                            <label id="priceLabel" for="price" data-error="ce champ ne prend que des chiffres">Prix
                                unitailre</label>
                        </div>
                    </div>
                    <div class="small-12 medium-12 large-2 p-r-10">
                        <div class="input-field">
                            <input  id="total_price" type="number" class="validate" value="0" disabled>
                            <label for="last_name" data-error="ce champ ne prend que des chiffres">Total</label>
                        </div>
                    </div>
                    <div class="small-12 medium-12 large-2 p-r-10 tar">
                        <button class="btn-floating btn-tiny waves-effect waves-black red" id="remove-this-item"><i
                                    class="material-icons">close</i></button>
                    </div>
                </div>

                <div style="display: none" id="row-template-tu">
                    <div class="row commande-list-item template-inside">
                        <div class="small-12 medium-12 large-4 p-r-10">
                            <div class="input-field">
                                <select name="productName" required onchange="addItem()">
                                    <option value="" disabled selected>Sélectioner votre produit</option>
                                    @foreach($products as $product)
                                        <option value="{{$product->catalogue_id}}">{{$product->libelle}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="small-12 medium-12 large-2 p-r-10">
                            <div class="input-field">
                                <input  id="shop-qnt" oninput="calculTotalShop()" type="number" class="validate">
                                <label for="shop-qnt" data-error="ce champ ne prend que des chiffres">Quantité</label>
                            </div>
                        </div>
                        <div class="small-12 medium-12 large-2 p-r-10">
                            <div class="input-field">
                                <input id="price" type="number" class="validate" disabled>
                                <label id="priceLabel" for="price" data-error="ce champ ne prend que des chiffres">Prix
                                    unitailre</label>
                            </div>
                        </div>
                        <div class="small-12 medium-12 large-2 p-r-10">
                            <div class="input-field">
                                <input id="total_price" type="number" class="validate" value="0" disabled>
                                <label for="total_price_label" data-error="ce champ ne prend que des chiffres">Total</label>
                            </div>
                        </div>
                        <div class="small-12 medium-12 large-2 p-r-10 tar">
                            <button class="btn-floating btn-tiny waves-effect waves-black red" id="remove-this-item"><i
                                        class="material-icons" onclick="closeItem(0)">close</i></button>
                        </div>
                    </div>
                </div>

            </div>
                <div style="display: none"  id="commamde_to_append" >
                    <div class="row commande-list-item ">
                        <div class="small-12 medium-12 large-4 p-r-10">
                            <div class="input-field">
                                <select name="" required onchange="addItem(this)">
                                    <option value="" disabled selected>Sélectioner votre produit</option>
                                    @foreach($products as $product)
                                        <option value="{{$product->catalogue_id}}">{{$product->libelle}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="small-12 medium-12 large-2 p-r-10">
                            <div class="input-field">
                                <input id="last_name" type="number" class="validate" required>
                                <label for="last_name" data-error="ce champ ne prend que des chiffres">Quantité</label>
                            </div>
                        </div>
                        <div class="small-12 medium-12 large-2 p-r-10">
                            <div class="input-field">
                                <input id="price" type="number" class="validate" disabled>
                                <label id="priceLabel" for="price" data-error="ce champ ne prend que des chiffres">Prix
                                    unitailre</label>
                            </div>
                        </div>
                        <div class="small-12 medium-12 large-2 p-r-10">
                            <div class="input-field">
                                <input id="last_name" type="number" class="validate" value="0" disabled>
                                <label for="last_name" data-error="ce champ ne prend que des chiffres">Total</label>
                            </div>
                        </div>
                        <div class="small-12 medium-12 large-2 p-r-10 tar">
                            <button class="btn-floating btn-tiny waves-effect waves-black red" id="remove-this-item"><i
                                        class="material-icons">close</i></button>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="small-12 medium-12 large-12 p-r-10 tar">
                        <a class="waves-effect waves-black hollow button secondary" id="add-more-commande-client">Ajouter
                            un produit</a>
                    </div>
                </div>
            <div class="total-paid">
                <div class="row">
                    <div class="small-12 medium-12 large-6 large-offset-4 tar">
                        <em>Total à payer</em>: <strong>390 <i>F cfa</i></strong>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal-footer">
            <a href="#!" class="modal-action modal-close waves-effect waves-black btn-flat grey button">ANNULER
                COMMANDE</a>

            <button class="modal-action waves-effect waves-black btn-flat success button" id=""
                    type='submit' onclick="createCommande()">ENVOYER LA COMMANDE
            </button>
        </div>

    </div>
</main>

<script src="../js/vendors/jquery.min.js"></script>
<script src="../js/vendors/foundation.min.js"></script>
<script src="../js/vendors/materialize.min.js"></script>
<script src="../js/app.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $('.form').submit(function (e) {
            e.preventDefault();
            var data = $(this).serialize();
            var catalogue_id = $(this).children('.catalogue_id').first().val();
            var client_id = $(this).children('.client_id').first().val();
            $('.bucket-items').prepend($('#row-template').html());
            var item_price = $(this).find('.item-price').eq(0).val();
            var item_name = $(this).find('.item-name').eq(0).val();
            var insertedElem = $('.bucket-items .row').eq(0);
            insertedElem.find('.item-name').text(item_name);
            insertedElem.find('.price-item').text(item_price);
            calculTotal()
            //$('.bucket-items .row').eq(0).last().remove();

            $.post('add_panier', {
                catalogue_id: catalogue_id,
                "_token": $("input[name=_token]").val(),
                client_id: client_id,
            }, function (data) {
                $("#card_items_number").text(data);
            });

        });

        $('.form-com').submit(function (e) {
            var productName = $(this).find('#name-product').val();
            var quantity = $(this).find('#quantity').val();
            console.log(productName, quantity);
        });

    });

    function showModalProduits() {
        calculTotal();
    }

    function orderbasket() {
        order();

//        $('#order').addClass('modal-action modal-close');
//        $('#order').click();
//        Materialize.toast('La commande 37DHS7 a été annuler', 4000, 'rounded')

    }

    function createShop()
    {
        calculTotalShop()
    }
    function calculTotalShop()
    {
        var total = 0;
        $('#commande-list .commande-list-item').each(function(i, elem){
            var factor = parseInt($(elem).find('#shop-qnt').eq(0).val()) || 0;
            var item_price = parseInt($(elem).find('#priceLabel').eq(0).text()) || 0;
            $(elem).find('#total_price').val((item_price * factor) + '');
            total += item_price * factor;
        });

        $('#create-shop .total-paid strong').text(total + ' F cfa');
    }

       function removeItem(panier)        //remove item panier
    {
        $(document).ready(function()
        {
            $.post('panier/remove',
                {
                    panier_id: panier[0],
                    "_token": $("input[name=_token]").val()
                }, function(data)
                {
                    $('.bucket-items .row').eq(panier[1]).remove();
                    $("#card_items_number").text(data);
                });
        });

    }

    function addItem(select)
    {
        $(".commande-list-item").not('.template-inside:eq(0)').find(".input-field select[name=productName]").each(function(i, select){
            var catalogue_id = $(select).val();
            $.post('product/price',
                {
                    catalogue_id: catalogue_id,
                    "_token": $("input[name=_token]").val()
                }, function(data)
                {
                    $(select).closest('.commande-list-item').find('.input-field #priceLabel').text(data);
                    calculTotalShop()
                });
        })
    }


    $('#remove-this-item').click(function() {
        $('.commande-list-item').remove();
        console.log('Item supprimé');
    });


    $('#add-more-commande-client').click(function() {
        $('#commande-list').append($('#row-template-tu').html());
        $('select').material_select();

    });

    function createCommande()
    {
        // save commande

        var array_quantite = [];
        var array_produit = [];

        $('#commande-list .commande-list-item').not('.template-inside:eq(0)').each(function(i, elem){
            var produit = $(elem).find(".input-field select[name=productName]").not('.template-inside:eq(0)').val();
            var  quantite = parseInt($(elem).find('#shop-qnt').eq(0).val());
            array_produit.push(produit)
            array_quantite.push(quantite);
        });

        $(document).ready(function()
        {
            $.post('commande', {
                produit: array_produit,
                quantite: array_quantite,
                "_token": $("input[name=_token]").val(),
            }, function (data) {
                $('#create-shop').modal('close')
            });

        });

    }

//    function closeItem(item)    //remove item commande
//    {
//        $(document).ready(function()
//        {
//            $('.bucket-items .row').eq(0).remove();
//        });
//    }
</script>

</body>
</html>