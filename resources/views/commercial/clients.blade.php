@extends('layouts.commercial.master-commercial')
@section('content')
    <main class="main">

        <p>&nbsp;</p>
        <div class="row">
            <div class="small-12 medium-12 large-12">
                <table class="unstriped">
                    <thead>
                    <tr>
                        <th>Prénom & Nom</th>
                        <th>Quartier</th>
                        <th>Adresse</th>
                        <th>Téléphone</th>
                        <th>Actions</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($clients as $client)
                        {{--{{ var_dump($client) }}--}}
                        <tr>
                            <td>
                                @if($client->prenom && $client->nom != null ) {{ ucfirst($client->prenom) }} {{ ucfirst($client->nom) }} @else Non renseigné  @endif</td>
                            <td>{{ ucfirst($client->quartier) }}</td>
                            <td> @if($client->adresse != null){{ ucfirst($client->adresse) }} @else Non renseigné @endif</td>
                            <td> {{ $client->phone1 }}</td>
                            <td><a href="#edit-client" class="modal-trigger" onclick="showModalEditClient({{$client->id}})"><i class="material-icons">remove_red_eye</i></a></td>
                            <td><a href="#create-commande" class="modal-trigger" onclick="commander({{$client->id}})"><i class="first-order">Commander</i></a></td>
                        </tr>

                        <div id="create-commande" class="modal modal-fixed-footer">
                            <div class="modal-content">
                                <h4>Nouvelle commande</h4>
                                {{--<h5>Sélectioner les produits</h5>--}}
                                <p>&nbsp;</p>
                                <div class="row">
                                    <div class="small-12 medium-12 large-6"><p></p></div>
                                    <div class="small-12 medium-12 large-2"><p></p></div>
                                    <div class="small-12 medium-12 large-2"><p></p></div>
                                    <div class="small-12 medium-12 large-2"></div>
                                </div>
                                <form action="" id="saveCommande">
                                    <div class="commande-list">
                                            <div class="row commande-list-item">
                                            <div class="small-12 medium-12 large-6 p-r-10">
                                                <div class="input-field">
                                                    <select  required name="quartier">
                                                        <option value="" disabled selected>Sélectioner votre quartier</option>
                                                        @foreach($produits as $produit)
                                                            <option value="{{$produit->id}}">{{$produit->nom}}</option>
                                                        @endforeach
                                                        {{--<label for="fournisseur"  data-error="..." >Quartier *</label>--}}
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="small-12 medium-12 large-2 p-r-10">
                                                <div class="input-field">
                                                    <label for="last_name"  data-error="ce champ ne prend que des chiffres" >Quantité</label>
                                                    <input id="last_name" type="number" class="validate">
                                                </div>
                                            </div>
                                            <div class="small-12 medium-12 large-2 p-r-10">
                                                <div class="input-field">
                                                    <label for="last_name"  data-error="ce champ ne prend que des chiffres" >Prix unitailre</label>
                                                    <input id="last_name" type="number" class="validate" value="1" oninput="calculTotal()">
                                                </div>
                                            </div>
                                            <div class="small-12 medium-12 large-2 p-r-10 tar">
                                                <button class="btn-floating btn-tiny waves-effect waves-black red" id="remove-this-item"><i class="material-icons">close</i></button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="small-12 medium-12 large-12 p-r-10 tar">
                                            <a class="waves-effect waves-black hollow button secondary" id="add-more-commande">Ajouter un produit</a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="modal-footer">
                                <a href="#!" class="modal-action modal-close waves-effect waves-black btn-flat grey button">ANNULER COMMANDE</a>
                                <button type="submit" form="saveCommande"  class="modal-action  waves-black btn-flat success button" onclick="">ENVOYER LA COMMANDE</button>
                            </div>
                        </div>
                    @endforeach
                    </tbody>
                </table>
                <div class="row">
                    <div class="small-12 medium-12 large-12">
                        <ul class="pagination text-center" role="navigation" aria-label="Pagination">
                            {{ $clients->links() }}
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div id="edit-client" class="modal modal-fixed-footer">
            <div class="modal-content">
                <h4>Edit client</h4>
                <form action="" id="saveEditClient">
                    <div class="row">
                        <div class="small-12 medium-12 large-8 large-offset-2">
                            <label for="editNom_client"  data-error="ce nom invalite" pattern="A-Za-z]{}" >Prénom *</label>
                            <div class="input-field">
                                <input id="editNom_client" type="text" class="validate" name="editNom_client" required>
                            </div>
                        </div>
                        {{ csrf_field() }}
                        <div class="small-12 medium-12 large-8 large-offset-2">
                            <label for="editPrenom_client"  data-error="ce prenom invalide" pattern="A-Za-z]{}" >Nom *</label>
                            <div class="input-field">
                                <input id="editPrenom_client" type="text" class="validate" name="editPrenom_clien" required>
                            </div>
                        </div>
                        <div class="small-12 medium-12 large-8 large-offset-2">
                            <label for="editTel_client"  data-error="numéro email invalide" >Téléphone *</label>
                            <div class="input-field">
                                <input id="editTel_client" type="tel" class="validate" name="editTel_client" pattern="[0-9]{2}[ \.\-]?[0-9]{3}[ \.\-]?[0-9]{2}[ \.\-]?[0-9]{2}" required>
                            </div>
                        </div>

                        <div class="small-12 medium-12 large-8 large-offset-2">
                            <label for="editAdresse_client"  data-error="cet adresse n'est pas valide" pattern="A-Za-z]{}" >Adresse *</label>
                            <div class="input-field">
                                <input id="editAdresse_client" type="text" class="validate" name="editAdresse_client">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <a href="#!"  class="modal-action modal-close waves-effect waves-black btn-flat success button">ANNULER</a>
                <button href="#!" type="submit" form="saveEditClient" class="modal-action waves-effect waves-black btn-flat grey button" >CRÉER</button>
            </div>
        </div>

    </main>
@section('script')
    <script src="../../js/vendors/jquery.min.js"></script>
    <script src="../../js/vendors/foundation.min.js"></script>
    <script src="../../js/vendors/materialize.min.js"></script>
    <script src="../../js/app.js"></script>
@endsection
<script src="../js/vendors/jquery.min.js"></script>
<script type="text/javascript">

    $('#add-more-commande').click(function() {
        $('.commande-list').append(`
      <div class="row commande-list-item">
        <div class="small-12 medium-12 large-6 p-r-10">
          <div class="input-field">
            <select  required name="quartier">
                <option value="" disabled selected>Sélectioner votre quartier</option>
                @foreach($produits as $produit)
                    <option value="{{$produit->id}}">{{$produit->nom}}</option>
                @endforeach
            </select>
          </div>
        </div>
        <div class="small-12 medium-12 large-2 p-r-10">
          <div class="input-field">
            <label for="last_name"  data-error="ce champ ne prend que des chiffres" >Quantité</label>
            <input id="last_name" type="number" class="validate">
          </div>
        </div>
        <div class="small-12 medium-12 large-2 p-r-10">
          <div class="input-field">
           <label for="last_name"  data-error="ce champ ne prend que des chiffres">Prix unitailre</label>
           <input id="last_name" type="number" value="1" class="validate" oninput="calculTotal()">
          </div>
        </div>
        <div class="small-12 medium-12 large-2 p-r-10 tar">
          <a class="btn-floating btn-tiny waves-effect waves-black red" id="remove-this-item"><i class="material-icons">close</i></a>
        </div>
      </div>
    `);

        $('#remove-this-item').click(function() {
            $('.commande-list-item').remove();
            console.log('Item supprimé');
        });
    });


    function commander(client_id)
    {
        $(document).ready(function(){

            $('#saveCommande').on('submit', function(event){
                event.preventDefault();

                var formData = new FormData($(this)[0]);
                alert(formData )

//                var formData = new FormData($(this)[0]);
//
//                $.ajax({
//                    url: 'commercial',
//                    type: 'POST',
//                    data: formData,
//                    async: false,
//                    success: function (data) {
//                        if(data=='error')
//                        {
//                            $('.error').append("<label style='color: red'>Ce numero existe deja</label>")
//                        }
//                        else if(data=='errorpassword')
//                        {
//                            $('.errorPassword').append("<label style='color: red'>les mots de passe ne correspondent pas</label>")
//                        }
//                        $('#new-seller').modal('close')
//                        location.reload();
//
//                    },
//                    cache: false,
//                    contentType: false,
//                    processData: false
//                });
//
//                return false;
            })
        })
    }

</script>
@endsection
