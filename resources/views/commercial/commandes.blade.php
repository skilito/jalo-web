@extends('layouts.commercial.master-commercial')
@section('content')
    <main class="main">
        <p>&nbsp;</p>
        <!-- Liste des commandes -->
        <div class="row">
            <div class="small-12 medium-12 large-12">
                @if($commandes[0] != [])
                <table class="unstriped">
                    <thead>
                    <tr>
                        <th>Référence</th>
                        <th>Date Commande</th>
                        <th>Boutiquier</th>
                        <th>Quartier</th>
                        <th>Téléphone Boutiquier</th>
                        <th>Statut</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($commandes as $commande)
                            <tr>
                                <td>{{$commande->reference}}</td>
                                <td>{{$commande->date_commande}}</td>
                                <td>{{ucfirst($commande->prenomBoutiquier)}} {{ucfirst($commande->nomBoutiquier)}}</td>
                                <td>{{$commande->quartier}}</td>
                                <td>{{$commande->telBoutiquier}}</td>
                                <td>{{$commande->etat}}</td>
                                <td><a href="{{ route('commande', ['id' => $commande->id]) }}" class="modal-trigger"><i class="material-icons">remove_red_eye</i></a></td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                @else
                    <h5 style="text-align: center">Pas de commande pour ce commercial</h5>
                @endif
            </div>
        </div>
    </main>
@endsection