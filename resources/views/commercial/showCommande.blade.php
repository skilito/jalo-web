@extends('layouts.commercial.master-commercial')
@section('content')
    <main class="main">
        <div class="modal modal-fixed-footer">
            <div class="modal-content">
                <header class="header-modal">
                    <h4>Confirmation</h4>
                    <date>7 Août 2017 - 12h02:46</date>
                </header>
                <p>&nbsp;</p>
                <h4>Nom du client</h4>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
                <div class="row">
                    <div class="small-12 medium-12 large-6"><strong>Produits</strong></div>
                    <div class="small-12 medium-12 large-2"><strong>2</strong></div>
                    <div class="small-12 medium-12 large-2"><strong>800</strong></div>
                    <div class="small-12 medium-12 large-2 tar"><strong>1 600 <i>F cfa</i></strong></div>
                </div>
                <div class="row">
                    <div class="small-12 medium-12 large-6"><strong>Produits</strong></div>
                    <div class="small-12 medium-12 large-2"><strong>2</strong></div>
                    <div class="small-12 medium-12 large-2"><strong>800</strong></div>
                    <div class="small-12 medium-12 large-2 tar"><strong>1 600 <i>F cfa</i></strong></div>
                </div>
                <p>&nbsp;</p>
                <div class="row">
                    <div class="small-12 medium-12 large-12 tar"><em>Total à payer</em>: <strong>1 600 <i>F cfa</i></strong></div>
                </div>
            </div>
            <div class="modal-footer">
                <a href="#!" class="modal-action modal-close waves-effect waves-black btn-flat grey button">FERMER</a>
                <a href="#!" class="modal-action modal-close waves-effect waves-black btn-flat success button">BOUCLER LA COMMANDE</a>
            </div>
        </div>
    </main>
@endsection