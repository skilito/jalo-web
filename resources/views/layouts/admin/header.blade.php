<header class="header">
    <div class="header-top">
        <div class="row">
            <div class="small-12 medium-4 large-2">
                @if(Request::is('admin/produits') || Request::is('admin/commandes') || Request::is('admin/dashboard') || Request::is('/admin/clients') )
                    <a href="/"><img src="../images/logo-yellow.jpeg" class="logo" alt=""></a>
                @elseif(Request::is('admin/client/*'))
                    <a href="/"><img src="../../../images/logo-yellow.jpeg" class="logo" alt=""></a>
                @else
                    <a href="/"><img src="../../images/logo-yellow.jpeg" class="logo" alt=""></a>
                @endif
            </div>
            <div class="small-12 medium-8 large-6">
                <div class="search">
                    @if(Request::is('admin/produits') )
                        <form action="/admin/produits" method="get">
                            <input type="search" class="search-input" placeholder="Rechercher un mot clé" name="search">
                            <button class="button button-search" >
                                @if(Request::is('admin/produits') || Request::is('admin/commandes') || Request::is('admin/dashboard') || Request::is('/admin/clients') )
                                    <img src="../images/search-icon.png" alt="Rechercher icon">
                                @elseif(Request::is('admin/client/*'))
                                    <img src="../../../images/search-icon.png" alt="Rechercher icon">
                                @else
                                    <img src="../../images/search-icon.png" alt="Rechercher icon">
                                @endif

                            </button>
                        </form>
                    @elseif(Request::is('admin/commandes') )
                        <form action="/admin/commandes" method="get">
                            <input type="search" class="search-input" placeholder="Rechercher un mot clé" name="search">
                            <button class="button button-search" >
                                @if(Request::is('admin/produits') || Request::is('admin/commandes') || Request::is('admin/dashboard') || Request::is('/admin/clients') )
                                    <img src="../images/search-icon.png" alt="Rechercher icon">
                                @elseif(Request::is('admin/client/*'))
                                    <img src="../../../images/search-icon.png" alt="Rechercher icon">
                                @else
                                    <img src="../../images/search-icon.png" alt="Rechercher icon">
                                @endif

                            </button>
                        </form>
                    @elseif(Request::is('admin/clients') )
                        <form action="/admin/clients" method="get">
                            <input type="search" class="search-input" placeholder="Rechercher un mot clé" name="search">
                            <button class="button button-search" >
                                @if(Request::is('admin/produits') || Request::is('admin/commandes') || Request::is('admin/dashboard') || Request::is('/admin/clients') )
                                    <img src="../images/search-icon.png" alt="Rechercher icon">
                                @elseif(Request::is('admin/client/*'))
                                    <img src="../../../images/search-icon.png" alt="Rechercher icon">
                                @else
                                    <img src="../../images/search-icon.png" alt="Rechercher icon">
                                @endif

                            </button>
                        </form>
                    @elseif(Request::is('admin/fournisseurs') )
                        <form action="/admin/fournisseurs" method="get">
                            <input type="search" class="search-input" placeholder="Rechercher un mot clé" name="search">
                            <button class="button button-search" >
                                @if(Request::is('admin/produits') || Request::is('admin/commandes') || Request::is('admin/dashboard') || Request::is('/admin/clients') )
                                    <img src="../images/search-icon.png" alt="Rechercher icon">
                                @elseif(Request::is('admin/client/*'))
                                    <img src="../../../images/search-icon.png" alt="Rechercher icon">
                                @else
                                    <img src="../../images/search-icon.png" alt="Rechercher icon">
                                @endif

                            </button>
                        </form>

                    @elseif(Request::is('admin/commerciaux') )
                        <form action="/admin/commerciaux" method="get">
                            <input type="search" class="search-input" placeholder="Rechercher un mot clé" name="search">
                            <button class="button button-search" >
                                @if(Request::is('admin/produits') || Request::is('admin/commandes') || Request::is('admin/dashboard') || Request::is('/admin/clients') )
                                    <img src="../images/search-icon.png" alt="Rechercher icon">
                                @elseif(Request::is('admin/client/*'))
                                    <img src="../../../images/search-icon.png" alt="Rechercher icon">
                                @else
                                    <img src="../../images/search-icon.png" alt="Rechercher icon">
                                @endif

                            </button>
                        </form>

                    @elseif(Request::is('admin/boutiques') )
                        <form action="/admin/boutiques" method="get">
                            <input type="search" class="search-input" placeholder="Rechercher un mot clé" name="search">
                            <button class="button button-search" >
                                @if(Request::is('admin/produits') || Request::is('admin/commandes') || Request::is('admin/dashboard') || Request::is('/admin/clients') )
                                    <img src="../images/search-icon.png" alt="Rechercher icon">
                                @elseif(Request::is('admin/client/*'))
                                    <img src="../../../images/search-icon.png" alt="Rechercher icon">
                                @else
                                    <img src="../../images/search-icon.png" alt="Rechercher icon">
                                @endif
                            </button>
                        </form>
                    @endif
                </div>
            </div>
            @if(Auth::user() != null )
                <div class="small-12 medium-12 large-3 large-offset-1">
                <div class="user-session">
                    <ul class="dropdown menu" data-dropdown-menu>
                        <li>
                            <a href="#" class="user-name"><i class="tiny material-icons">person</i>{{ucfirst(Auth::user()->prenom)}} {{ ucfirst(Auth::user()->nom)}}</a>
                            <ul class="menu">
                                <li><a href="#"><i class="tiny material-icons">person</i> Profil</a></li>
                                <li><a href="#change-password" class=" waves-effect waves-black modal-trigger"><i class="tiny material-icons">settings</i> Changer mot passe</a></li>
                                <li><a href="/logout"><i class="tiny material-icons">exit_to_app</i> Déconnexion</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
            @endif
        </div>
    </div>
    <div class="row">
        <div class="title-bar" data-responsive-toggle="example-animated-menu" data-hide-for="medium">
            <button class="menu-icon" type="button" data-toggle></button>
            <div class="title-bar-title">Menu</div>
        </div>

        <div class="top-bar" id="example-animated-menu" data-animate="hinge-in-from-top spin-out">
            @if(Auth::user() != null)
            <div class="top-bar-left">
                <ul class="menu">
                    <li>
                        <a href="/admin/dashboard" class="@if(Request::is('admin/dashboard')) is-active @endif waves-effect waves-yellow"><i class="tiny material-icons">dashboard</i> <span>Tableau de Bord</span></a></li>
                    <li><a href="/admin/produits" class="@if(Request::is('admin/produits')) is-active @endif waves-effect waves-yellow"><i class="tiny material-icons">shop</i> <span>Produits</span></a></li>
                    <li><a href="/admin/commandes" class="@if(Request::is('admin/commandes')) is-active @endif waves-effect waves-yellow"><i class="tiny material-icons">shopping_cart</i> <span>Commandes</span></a></li>
                    <li><a href="/admin/clients" class="@if(Request::is('/admin/clients')) is-active @endif waves-effect waves-yellow"><i class="tiny material-icons">group</i><span>Clients</span></a></li>
                    <li><a href="/admin/fournisseurs" class="@if(Request::is('/admin/fournisseurs')) is-active @endif waves-effect waves-yellow"><i class="tiny material-icons">local_shipping</i> <span>Fournisseurs</span></a></li>
                    <li><a href="/admin/commerciaux" class="@if(Request::is('/admin/commerciaux')) is-active @endif waves-effect waves-yellow"><i class="tiny material-icons">person_pin_circle</i> <span>Commerciaux</span></a></li>
                    <li><a href="/admin/boutiques" class="@if(Request::is('/admin/boutiques')) is-active @endif waves-effect waves-yellow"><i class="tiny material-icons">local_shipping</i><span>Boutiques</span></a></li>
                    <li><a href="/admin/categories" class="@if(Request::is('/admin/categories')) is-active @endif waves-effect waves-yellow">{{--<i class="tiny material-icons">local_shipping</i>--}}<span>Catégories</span></a></li>
                </ul>
            </div>
            @endif
            <div class="top-bar-right">
                    <ul class="menu">
                    @if (Request::is('admin/dashboard'))
                        <li>
                            <a href="#add-admin" class="add-bucket-btn-tiny waves-effect waves-black modal-trigger">
                                Ajouter Aministrateur <span class="btn-floating btn-tiny waves-effect waves-yellow white"><i class="material-icons">add</i></span></a>
                        </li>
                    @elseif(Request::is('admin/produits'))
                        <li>
                            <a href="#add-product" class="add-bucket-btn-tiny waves-effect waves-black modal-trigger">
                                Ajouter Produit <span class="btn-floating btn-tiny waves-effect waves-yellow white"><i class="material-icons">add</i></span></a>
                        </li>
                    @elseif(Request::is('admin/fournisseurs'))
                        <a href="#new-client" class="add-bucket-btn-tiny waves-effect waves-black modal-trigger">
                            Ajouter Fournisseur <span class="btn-floating btn-tiny waves-effect waves-yellow white"><i class="material-icons">add</i></span></a>
                    @elseif(Request::is('admin/commerciaux'))
                        <a href="#new-client" class="add-bucket-btn-tiny waves-effect waves-black modal-trigger">
                            Ajouter Commercial <span class="btn-floating btn-tiny waves-effect waves-yellow white"><i class="material-icons">add</i></span></a>
                   @elseif(Request::is('admin/boutiques'))
                        <a href="#new-boutique" class="add-bucket-btn-tiny waves-effect waves-black modal-trigger">
                            Ajouter Boutique <span class="btn-floating btn-tiny waves-effect waves-yellow white"><i class="material-icons">add</i></span>
                        </a>
                        @elseif(Request::is('admin/categories'))
                            <a href="/admin/category/new" class="add-bucket-btn-tiny waves-effect waves-black modal-trigger">
                                Ajouter Categorie <span class="btn-floating btn-tiny waves-effect waves-yellow white"><i class="material-icons">add</i></span>
                            </a>

                    @endif
                </ul>
            </div>
        </div>
    </div>

</header>

<div id="change-password" class="modal modal-fixed-footer">
    <div class="modal-content">
        <header class="header-modal">
            <h4>Changer mot de passe</h4>
        </header>
        <p>&nbsp;</p>
        <form id="saveNewPassword">
            <div class="row">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="small-12 medium-12 large-8 large-offset-2">
                    <div class="input-field">
                        <label for="old-password"  data-error="..." >Ancien mot de passe *</label>
                        <input id="old-password" type="password" class="validate" name="old-password" required>
                    </div>
                    <div class="error-old-password">
                    </div>
                </div>
                <div class="small-12 medium-12 large-8 large-offset-2">
                    <div class="input-field">
                        <label for="new-password"  data-error="..." >Nouveau mot de passe *</label>
                        <input id="new-password" type="password" class="validate" name="password" required>
                    </div>
                </div>
                <div class="small-12 medium-12 large-8 large-offset-2">
                    <div class="input-field">
                        <label for="password-confirm"  data-error="..." >Confirmer mot de passe *</label>
                        <input id="password-confirm" type="password" class="validate" name="password_confirmation" required>
                    </div>
                    <div class="error-confirmation">

                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="modal-footer">
        <a href="#!" class="modal-action modal-close waves-effect waves-black btn-flat success button">ANNULER</a>
        <button href="#!"  class="btn-flat grey button" form="saveNewPassword" type="submit">s'auvegarder</button>
    </div>
</div>

@if(Request::is('admin/boutiques'))
    <div id="new-boutique" class="modal modal-fixed-footer">
    <div class="modal-content">
        <h4>Nouveau Boutique</h4>
        <form action="" id="save-boutique">
            <div class="row">
                {{ csrf_field() }}
                <div class="small-12 medium-12 large-8 large-offset-2">
                    <div class="input-field">
                        <select name="boutiquier" required>
                            <option value="" disabled selected>Sélectioner le boutiquier</option>
                            @foreach($boutiquiers as $boutiquier)
                                <option value="{{$boutiquier->id}}">{{$boutiquier->prenom}} {{$boutiquier->nom}}</option>
                            @endforeach
                            {{--@if($errors->has('quartier')) {{ $errors->first('quartier') }}@endif--}}
                        </select>
                    </div>
                </div>

                <div class="small-12 medium-12 large-8 large-offset-2">
                    <div class="input-field">
                        <select name="commercial" required>
                            <option value="" disabled selected>Sélectioner le commercial</option>
                            @foreach($commerciaux as $commercial)
                                <option value="{{$commercial->id}}">{{$commercial->prenom}} {{$commercial->nom}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="modal-footer">
        <a href="#!" class="modal-action modal-close waves-effect waves-black btn-flat grey button">ANNULER</a>
        {{--<a href="#!" class="modal-action modal-close waves-effect waves-black btn-flat success button" onclick="Materialize.toast('client créé', 4000, 'rounded')">CRÉER CLIENT</a>--}}
        <button href="#!" class="modal-actionwaves-effect waves-black btn-flat green button" type="submit" form="save-boutique">CRÉER</button>
    </div>
</div>

    <script>

        $(document).ready(function(){

            $('#save-boutique').on('submit', function(event)
            {
                event.preventDefault();

                var formData = new FormData($(this)[0]);

                $.ajax({
                    url: 'boutique',
                    type: 'POST',
                    data: formData,
                    async: false,
                    success: function (data) {

                        $('#new-boutique').modal('close')
                        location.reload();
                    },
                    cache: false,
                    contentType: false,
                    processData: false
                });

                return false;
            })
        })
    </script>

@endif

@if(Request::is('admin/produits') || Request::is('admin/commandes'))
<!-- Modal Structure -->
<div id="add-product" class="modal modal-fixed-footer">
    <div class="modal-content">
        <header class="header-modal">
            <h4>Nouveau Produit</h4>
        </header>
        <p>&nbsp;</p>
        <form action="" id="saveProduit">
            <div class="row">

                <div class="small-12 medium-12 large-8 large-offset-2">
                    <div class="input-field">
                        <label for="nomProduit"  data-error="..."  >Nom Produit *</label>
                        <input id="nomProduit" type="text" class="validate" name="nomProduit" required>
                    </div>
                </div>
                <div class="small-12 medium-12 large-8 large-offset-2">
                    <div class="input-field">
                        <label for="descriptionProduit"  data-error="...">Description Produit *</label>
                        <input id="descriptionProduit" type="text" class="validate" name="descriptionProduit" required>
                    </div>
                </div>
                <div class="small-12 medium-12 large-8 large-offset-2">
                    <div class="input-field">
                        <select  id="categorie" required name="categorie">
                            <option value="" disabled selected>Categorie</option>
                            @foreach($categories as $categorie)
                                <option value="{{$categorie->id}}">{{ucfirst($categorie->libelle)}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                {{ csrf_field() }}
                <div class="small-12 medium-12 large-8 large-offset-2">
                    <div class="input-field">
                        <label for="prixProduit"  data-error="..." >Prix *</label>
                        <input id="prixProduit" type="number" class="validate" name="prixProduit" required>
                    </div>
                </div>

                <div class="small-12 medium-12 large-8 large-offset-2">
                    <div class="input-field">
                        <select  id="fournisseur" required name="fournisseur">
                            <option value="" disabled selected>Fournisseur</option>
                            @foreach($fournisseurs as $fournisseur)
                                <option value="{{$fournisseur->id}}">{{ucfirst($fournisseur->prenom)}} {{ucfirst($fournisseur->nom)}} </option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="small-12 medium-12 large-8 large-offset-2">
                    <div class="row no-margin">
                        <div class="small-12 medium-12 large-5">
                            <div class="input-field">
                                <label for="jaloPourcentage"  data-error="..." >Pourcentage JALO</label>
                                <input id="jaloPourcentage" type="number" class="validate" name="jaloPourcentage">
                            </div>
                        </div>
                        <div class="small-12 medium-12 large-5 large-offset-2">
                            <div class="input-field">
                                <label for="boutiquierPourcentage"  data-error="..." >Pourcentage Boutiquier</label>
                                <input id="boutiquierPourcentage" type="number" class="validate" name="boutiquierPourcentage">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="small-12 medium-12 large-6 large-offset-2">
                    <div class="file-field input-field">
                        {{--<div class="btn lh13 bgc-yellow">--}}
                            {{--<span>Photo Produit</span>--}}
                        {{--</div>--}}
                        <div class="file-path-wrapper">
                            <input name="new_photo" required type="file">
                            {{--<input class="file-path validate" type="text" required>--}}
                        </div>
                    </div>
                </div>

            </div>
        </form>
    </div>
    <div class="modal-footer">
        <a href="#!" class="modal-action modal-close waves-effect waves-black btn-flat success button">ANNULER</a>
        <button href="#!" class="modal-action waves-effect waves-black btn-flat grey button" form ="saveProduit" type="submit" data-disable-with="<i class='fa fa-refresh fa-spin'></i> loading.....">CRÉER</button>
    </div>
</div>

<script>

    $(document).ready(function(){

        $('#saveProduit').on('submit', function(event)
        {
            event.preventDefault();

            var formData = new FormData($(this)[0]);

            $.ajax({
                url: 'produit',
                type: 'POST',
                data: formData,
                async: false,
                success: function (data) {
                    $('#add-product').modal('close')
                    location.reload();
                },
                cache: false,
                contentType: false,
                processData: false
            });

            return false;
        })
    })
</script>
@endif

@if(Request::is('admin/clients') || Request::is('admin/fournisseurs') || Request::is('admin/commerciaux'))
    <div id="new-client" class="modal modal-fixed-footer">
        <div class="modal-content">
            <h4>Nouveau @if(Request::is('admin/clients'))client @elseif(Request::is('admin/fournisseurs')) fournisseur @else commercial @endif</h4>
            <form action="" id="save-client">
                <div class="row">
                    <div class="small-12 medium-12 large-8 large-offset-2">
                        <div class="input-field">
                            <label for="prenom_client"  data-error="Ce prénom n'est pas valide" >Prénom *</label>
                            <input id="prenom_client" type="text" class="validate" name="prenom_client" pattern="A-Za-z]{}"  required>
                        </div>
                    </div>
                    @if(Request::is('admin/clients'))
                        <input type="hidden" name="role" value="client">
                    @elseif(Request::is('admin/fournisseurs'))
                        <input type="hidden" name="role" value="fournisseur">
                    @else
                        <input type="hidden" name="role" value="commercial">
                    @endif
    {{ csrf_field() }}
    <div class="small-12 medium-12 large-8 large-offset-2">
        <div class="input-field">
            <label for="nom_client"  data-error="ce nom n'est pas valide" >Nom *</label>
            <input id="nom_client" type="text" class="validate" name="nom_client"  pattern="A-Za-z]{}" required >
        </div>
    </div>
    <div class="small-12 medium-12 large-8 large-offset-2">
        <div class="input-field">
            <label for="tel_client"  data-error="invalide numero telephone" >Téléphone *</label>
            <input id="tel_client" type="tel" class="validate" name="tel_client"  pattern="[0-9]{2}[ \.\-]?[0-9]{3}[ \.\-]?[0-9]{2}[ \.\-]?[0-9]{2}" required>
        </div>
    </div>
    <div class="small-12 medium-12 large-8 large-offset-2">
        <div class="input-field">
            <label for="email_client"  data-error="adresse email invalide" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$">email </label>
            <input id="email_client" type="email" class="validate" name="email_client">
        </div>
    </div>
                    <div class="small-12 medium-12 large-8 large-offset-2">
                        <div class="input-field">
                            <label for="password_commercial"  data-error="..." >Mot de Passe *</label>
                            <input id="password_commercial" type="password" class="validate" name="password" required>
                        </div>
                        <div class="errorPassword">
                        </div>
                    </div>
                    <div class="small-12 medium-12 large-8 large-offset-2">
                        <div class="input-field">
                            <label for="confirpassword_commercial"  data-error="..." >Confirmer mot de passe *</label>
                            <input id="confirpassword_commercial" type="password" class="validate" name="confirpassword" required>
                        </div>
                    </div>
    <div class="small-12 medium-12 large-8 large-offset-2">
        <div class="input-field">
            <label for="adresse_client"  data-error="adresse non valide" >Adresse </label>
            <input id="adresse_client" type="tel" class="validate" name="adresse_client">
        </div>
    </div>
    <div class="small-12 medium-12 large-8 large-offset-2">
        <div class="input-field">
            <select name="quartier" required>
                <option value="" disabled selected>Sélectioner votre quartier</option>
                @foreach($quartiers as $quartier)
                    <option value="{{$quartier->id}}">{{$quartier->nom}}</option>
                @endforeach
                {{--@if($errors->has('quartier')) {{ $errors->first('quartier') }}@endif--}}
            </select>
        </div>
    </div>
</div>
</form>
</div>
<div class="modal-footer">
<a href="#!" class="modal-action modal-close waves-effect waves-black btn-flat grey button">ANNULER</a>
{{--<a href="#!" class="modal-action modal-close waves-effect waves-black btn-flat success button" onclick="Materialize.toast('client créé', 4000, 'rounded')">CRÉER CLIENT</a>--}}
<button href="#!" class="modal-actionwaves-effect waves-black btn-flat grey button" type="submit" form="save-client">CRÉER</button>
</div>
</div>

<script>

$('#save-client').on('submit', function(event)
{
    event.preventDefault();

    var formData = new FormData($(this)[0]);

    $(document).ready(function(){

    $.ajax({
        url: 'client',
        type: 'POST',
        data: formData,
        async: false,
        success: function (data) {
         if(data=='errorpassword')
            {
                $('.errorPassword').append("<label style='color: red'>les mots de passe ne correspondent pas</label>")
            }
            else
         {
             $('#new-client').modal('close')
             location.reload();
         }
        },
        cache: false,
        contentType: false,
        processData: false
    });

    return false;
})
})
</script>
@endif

<script>

    $(document).ready(function(){

        $('#saveNewPassword').on('submit', function(event)
        {
            event.preventDefault();

            var formData = new FormData($(this)[0]);

            $.ajax({
                url: 'change-password',
                type: 'POST',
                data: formData,
                async: false,
                success: function (data)
                {
                    if(data == 'error')
                    {
                        if($(".error-old-password").children().length < 1)
                        {
                            $('.error-old-password').append("<label style='color: red'>le mot de passe saisi est incorrecte</label>")
                        }
                    }
                    else if(data == 'error-confirmation'){
                        if($(".error-confirmation").children().length < 1) {
                            $('.error-confirmation').append("<label style='color: red'>les mots de passe ne correspondent pas</label>")
                        }
                    }
                    else if(data == 'ok')
                    {
                        location.reload();
                    }
                },
                cache: false,
                contentType: false,
                processData: false
            });

            return false;
        })
    })
</script>

