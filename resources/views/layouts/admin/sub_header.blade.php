<header class="header">
    <div class="header-top">
        <div class="row">
            <div class="small-12 medium-4 large-2">
                <img src="../images/logo-yellow.jpeg" class="logo" alt="">
            </div>
            <div class="small-12 medium-8 large-6">
                <div class="search">
                    <form action="">
                        <input type="search" class="search-input" placeholder="Rechercher un mot clé">
                        <button class="button button-search"><img src="../images/search-icon.png" alt="Rechercher icon"></button>
                    </form>
                </div>
            </div>
            <div class="small-12 medium-12 large-3 large-offset-1">
                <div class="user-session">
                    <ul class="dropdown menu" data-dropdown-menu>
                        <li>
                            <a href="#" class="user-name"><i class="tiny material-icons">person</i>Administrateur</a>
                            <ul class="menu">
                                <li><a href="#"><i class="tiny material-icons">person</i> Profil</a></li>
                                <li><a href="#"><i class="tiny material-icons">settings</i> Paramettre</a></li>
                                <li><a href="/sign-in.html"><i class="tiny material-icons">exit_to_app</i>Déconnexion</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="title-bar" data-responsive-toggle="example-animated-menu" data-hide-for="medium">
            <button class="menu-icon" type="button" data-toggle></button>
            <div class="title-bar-title">Menu</div>
        </div>

        <div class="top-bar" id="example-animated-menu" data-animate="hinge-in-from-top spin-out">
            <div class="top-bar-left">
                <ul class="menu">
                    <li><a href="dashboard.html" class="is-active waves-effect waves-yellow"><i class="tiny material-icons">dashboard</i> <span>Tableau de bord</span></a></li>
                    <li><a href="produits.html" class="waves-effect waves-yellow"><i class="tiny material-icons">shop</i> <span>Produits</span></a></li>
                    <li><a href="commandes.html" class="waves-effect waves-yellow"><i class="tiny material-icons">shopping_cart</i> <span>Commandes</span></a></li>
                    <li><a href="clients.html" class="waves-effect waves-yellow"><i class="tiny material-icons">group</i><span>Clients</span></a></li>
                    <li><a href="fournisseurs.html" class="waves-effect waves-yellow"><i class="tiny material-icons">local_shipping</i> <span>Fournisseurs</span></a></li>
                    <li><a href="commerciaux.html" class="waves-effect waves-yellow"><i class="tiny material-icons">person_pin_circle</i> <span>Commerciaux</span></a></li>
                </ul>
            </div>
            <div class="top-bar-right">
                <ul class="menu">
                    <li>
                        <a href="#add-admin" class="add-bucket-btn-tiny waves-effect waves-black modal-trigger"> Ajouter Aministrateur <span class="btn-floating btn-tiny waves-effect waves-yellow white"><i class="material-icons">add</i></span></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</header>