    <!DOCTYPE html>
    <html class="no-js" lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>JALÔ, Tout près de chez vous!</title>
        <link rel="icon" type="image/jpeg" href="images/logo-yellow.jpeg">
        <link rel="stylesheet" href="css/app.css">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
    </head>
    <body>

    @yield('content')

    <!-- Scripts -->
    <!-- Scripts -->
    <script src="js/vendors/jquery.min.js"></script>
    <script src="js/vendors/foundation.min.js"></script>
    <script src="../js/vendors/materialize.min.js"></script>
    <script src="js/app.js"></script>
        <!-- Flickity(slider) -->
    <script src="js/vendors/flickity.pkgd.min.js"></script>
    <script>
      $('.main-carousel').flickity({
        // options
        cellAlign: 'left',
        contain: true,
        prevNextButtons: false,
        autoPlay: true
      });
    </script>
    </body>
</html>
