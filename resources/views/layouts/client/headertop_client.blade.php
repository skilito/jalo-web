<div class="header-top">
        <div class="row">
          <div class="small-12 medium-4 large-2">
            <a href="/"><img src="../images/logo-yellow.jpeg" class="logo" alt=""></a>
          </div>
          <div class="small-12 medium-8 large-6">
            <div class="search">
              <form action="">
                <input type="search" class="search-input" placeholder="Rechercher un mot clé">
                <button class="button button-search"><img src="../images/search-icon.png" alt="Rechercher icon"></button>
              </form>
            </div>
          </div>
          <div class="small-12 medium-12 large-3 large-offset-1">
            <div class="user-session">
              <ul class="dropdown menu" data-dropdown-menu>
                <li><a href="" class="notification-is-active"><i class="tiny material-icons">notifications</i><span></span></a></li>
                <li>
                  <a href="#" class="user-name"><i class="tiny material-icons">person</i>{{ ucfirst(Auth::user()->nom) }}</a>
                  <ul class="menu">
                    <li><a href="#"><i class="tiny material-icons">person</i> Profil</a></li>
                    <li><a href="#"><i class="tiny material-icons">settings</i> Paramettre</a></li>
                    <li><a href="/logout"><i class="tiny material-icons">exit_to_app</i> Déconnexion</a></li>
                  </ul>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>