<header class="header">
    <div class="header-top">
        <div class="row">
            <div class="small-12 medium-4 large-2">
                <a href="/"><img src="../images/logo-yellow.jpeg" class="logo" alt=""></a>
            </div>
            <div class="small-12 medium-8 large-6">
                <div class="search">
                    @if(Request::is('commercial/catalogues'))
                        <form action="/commercial/catalogues" method="get">
                            <input type="search" class="search-input" placeholder="Rechercher un mot clé" name="search">
                            <button class="button button-search"><img src="../images/search-icon.png" alt="Rechercher icon"></button>
                        </form>
                    @elseif(Request::is('commercial/commandes'))
                        <form action="/commercial/commandes" method="get">
                            <input type="search" class="search-input" placeholder="Rechercher un mot clé" name="search">
                            <button class="button button-search"><img src="../images/search-icon.png" alt="Rechercher icon"></button>
                        </form>
                    @elseif(Request::is('commercial/clients'))
                        <form action="/commercial/clients" method="get">
                            <input type="search" class="search-input" placeholder="Rechercher un mot clé" name="search">
                            <button class="button button-search"><img src="../images/search-icon.png" alt="Rechercher icon"></button>
                        </form>
                    @elseif(Request::is('commercial/fournisseurs'))
                        <form action="/commercial/fournisseurs" method="get">
                            <input type="search" class="search-input" placeholder="Rechercher un mot clé" name="search">
                            <button class="button button-search"><img src="../images/search-icon.png" alt="Rechercher icon"></button>
                        </form>
                    @endif
                </div>
            </div>
            <div class="small-12 medium-12 large-3 large-offset-1">
                <div class="user-session">
                    <ul class="dropdown menu" data-dropdown-menu>
                        <li>
                            <a href="#" class="user-name"><i class="tiny material-icons">person</i>{{ ucfirst(Auth::user()->prenom)}} {{ucfirst(Auth::user()->nom)}}</a>
                            <ul class="menu">
                                <li><a href="#"><i class="tiny material-icons">person</i> Profil</a></li>
                                <li><a href="#"><i class="tiny material-icons">settings</i> Paramettre</a></li>
                                <li><a href="/logout"><i class="tiny material-icons">exit_to_app</i> Déconnexion</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="title-bar" data-responsive-toggle="example-animated-menu" data-hide-for="medium">
            <button class="menu-icon" type="button" data-toggle></button>
            <div class="title-bar-title">Menu</div>
        </div>

        <div class="top-bar" id="example-animated-menu" data-animate="hinge-in-from-top spin-out">
            <div class="top-bar-left">
                <ul class="menu">
                    <li><a href="{{route('dashbord-commercial', ['category' => 'all'])}}" class="@if(Request::is('commercial/catalogues')) is-active @endif waves-effect waves-yellow"><i class="tiny material-icons">view_module</i> <span>Catalogue</span></a></li>
                    <li><a href="{{route('commercial-commande')}}" class="@if(Request::is('commercial/commandes')) is-active @endif waves-effect waves-yellow"><i class="tiny material-icons">shopping_cart</i> <span>Commandes</span></a></li>
                    <li><a href="{{route('commercial-clients')}}" class="@if(Request::is('commercial/clients')) is-active @endif waves-effect waves-yellow"><i class="tiny material-icons">group</i> <span>Mes clients</span></a></li>
                    <li><a href="{{ route('commercial-fournisseurs') }}" class="@if(Request::is('commercial/fournisseurs')) is-active @endif waves-effect waves-yellow"><i class="tiny material-icons">local_shipping</i> <span>Fournisseurs</span></a></li>
                    {{--<li><a href="dashboard.html" class="waves-effect waves-yellow"><i class="tiny material-icons">dashboard</i> <span>Dashboard</span></a></li>--}}
                </ul>
            </div>

            <div class="top-bar-right">
                <ul class="menu">
                    @if((Request::is('commercial/catalogues')))
                        <li>
                            <a href="#add-product" class="add-bucket-btn-tiny waves-effect waves-black modal-trigger"> Ajouter Produit <span class="btn-floating btn-tiny waves-effect waves-yellow white"><i class="material-icons">add</i></span></a>
                        </li>
                     @endif
                </ul>
            </div>
        </div>
    </div>
</header>

@if(Request::is('commercial/catalogues'))
    <!-- Modal Structure -->
    <div id="add-product" class="modal modal-fixed-footer">
        <div class="modal-content">
            <header class="header-modal">
                <h4>Nouveau Produit</h4>
            </header>
            <p>&nbsp;</p>
            <form action="" id="saveProduit">
                <div class="row">

                    <div class="small-12 medium-12 large-8 large-offset-2">
                        <div class="input-field">
                            <input id="nomProduit" type="text" class="validate" name="nomProduit" required>
                            <label for="nomProduit"  data-error="..."  >Nom Produit *</label>
                        </div>
                    </div>
                    <div class="small-12 medium-12 large-8 large-offset-2">
                        <div class="input-field">
                            <select  id="categorie" required name="categorie">
                                <option value="" disabled selected>Categorie</option>
                                @foreach($categories as $categorie)
                                    <option value="{{$categorie->id}}">{{ucfirst($categorie->libelle)}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    {{ csrf_field() }}
                    <div class="small-12 medium-12 large-8 large-offset-2">
                        <div class="input-field">
                            <input id="prixProduit" type="number" class="validate" name="prixProduit" required>
                            <label for="prixProduit"  data-error="..." >Prix *</label>
                        </div>
                    </div>
                    <div class="small-12 medium-12 large-8 large-offset-2">
                        <div class="input-field">
                            <select  id="fournisseur" required name="fournisseur">
                                <option value="" disabled selected>Fournisseur</option>
                                @foreach($fournisseurs as $fournisseur)
                                    <option value="{{$fournisseur->id}}">{{ucfirst($fournisseur->prenom)}} {{ucfirst($fournisseur->nom)}} </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="small-12 medium-12 large-8 large-offset-2">
                        <div class="row no-margin">
                            <div class="small-12 medium-12 large-5">
                                <div class="input-field">
                                    <input id="jaloPourcentage" type="number" class="validate" name="jaloPourcentage">
                                    <label for="jaloPourcentage"  data-error="..." >Pourcentage JALO</label>
                                </div>
                            </div>
                            <div class="small-12 medium-12 large-5 large-offset-2">
                                <div class="input-field">
                                    <input id="boutiquierPourcentage" type="number" class="validate" name="boutiquierPourcentage">
                                    <label for="boutiquierPourcentage"  data-error="..." >Pourcentage Boutiquier</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="small-12 medium-12 large-6 large-offset-2">
                        <div class="file-field input-field">
                            <div class="btn lh13 bgc-yellow">
                                <span>Photo Produit</span>
                                <input name="new_photo" required type="file">
                            </div>
                            <div class="file-path-wrapper">
                                <input class="file-path validate" type="text" required>
                            </div>
                        </div>
                    </div>

                </div>
            </form>
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-action modal-close waves-effect waves-black btn-flat success button">ANNULER</a>
            <button href="#!" class="modal-action waves-effect waves-black btn-flat grey button" form ="saveProduit" type="submit">CRÉER</button>
        </div>
    </div>

    <script>

        $(document).ready(function(){

            $('#saveProduit').on('submit', function(event)
            {
                event.preventDefault();

                var formData = new FormData($(this)[0]);

                $.ajax({
                    url: '/admin/produit',
                    type: 'POST',
                    data: formData,
                    async: false,
                    success: function (data) {

                        $('#add-product').modal('close')
                        location.reload();
                    },
                    cache: false,
                    contentType: false,
                    processData: false
                });

                return false;
            })
        })
    </script>
@endif
